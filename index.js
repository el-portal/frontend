const express = require('express')
const app = express()
const {logger} = require('./logger')

const port = process.env.FRONTEND_PORT

process.on('uncaughtException', err => {
    logger.error({action: 'Fatal error', data:{error:err}})
    setTimeout(() => { process.exit(0) }, 1000).unref() 
})

process.on('unhandledRejection', (err, err2) => {
    logger.error({action: 'Fatal error', data:{error:err}})
    setTimeout(() => { process.exit(0) }, 1000).unref() 
})

app.use(express.static('build'))

app.use(function(req, res){ 
    logger.info({ action: 'Service front required'}) 
    res.sendFile(__dirname+'/build/index.html')
});

app.listen(port, function () { logger.info({ action: 'Service running on port ' + port}) } )
