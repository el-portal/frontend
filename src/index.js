import React, { Component } from "react"
import ReactDOM from 'react-dom'
import { BrowserRouter as Router, Route, Switch } from "react-router-dom"
import { consoleLogDebugCreator } from './logger'
import { ThemeContext } from "./context"
import { themes } from './themes'
import Home from "./pages/home"
import Page404 from "./pages/page404/page404"

const verbosityLevel = 0
const consoleLogDebug = consoleLogDebugCreator(verbosityLevel)

/*
if (process.env.NODE_ENV === 'production') {
  consoleLogDebug = () => {}
}
*/
class App extends Component { 
  constructor(props) {
    super(props);
    this.state = {
      theme:themes[1],
      username: localStorage.getItem('username') || sessionStorage.getItem('username'),
      name: localStorage.getItem('name') || sessionStorage.getItem('name'),
      lastname:  localStorage.getItem('lastname') || sessionStorage.getItem('lastname'),
    };
    this.changeTheme= this.changeTheme.bind(this);
  }

  componentDidMount(){
    consoleLogDebug(process.env.REACT_APP_BACK_URL, 1)
    consoleLogDebug('Mounting index')
    const themeName = localStorage.getItem('themeName')
    if (themeName!==null){
      themes.forEach(theme => {
        if(theme.name===themeName){
          consoleLogDebug('Theme:' + theme.name, 1)
          this.setState({theme:theme})
        }
      });
    }
  }

  changeTheme(themeName){
    consoleLogDebug('Changing theme to ' + themeName)
    themes.forEach(theme => {
      if(theme.name===themeName){
        this.setState({theme:theme})
        localStorage.setItem('themeName', themeName);
      }
    });
  }

  render() {
    consoleLogDebug('Rendering index', 1)
    return(
      <ThemeContext.Provider value={{theme:this.state.theme, changeTheme: this.changeTheme}}>
        <Router>
          <Switch>

            <Route exact path="/" component={Home} />
            <Route exact path="/home" component= {Home} />

            <Route component= {Page404}/>
          </Switch>
        </Router>
      </ThemeContext.Provider>
    ) 
  }
}

ReactDOM.render(<App />, document.getElementById('root'));
