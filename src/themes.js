exports.themes = [

    {
        name: 'white',

        primary: '#b71b54',
        secondary: 'white',

        menu_background: '#f4f4f5',
        menu_color: '#7e8190',
        menu_color_selected: '#b71b54',
        menu_color_hover: '#bb7991',
        
        content_background: 'white',
        content_color: '#60616d',
        content_color_selected: 'black',

        borders: '#cacaca',

        error: 'red',
        success: 'green',
    },

    { 
        name: 'dark',

        primary: '#b71b54',
        secondary: 'white',

        menu_background: 'black',
        menu_color: '#767888',
        menu_color_selected: 'white',
        menu_color_hover: '#b0b2bf',

        content_background: '#1c1e2d',
        content_color: '#767888',
        content_color_selected: 'white',

        borders: '#333543',
        
        error: 'red',
        success: 'green',
    },
]
 