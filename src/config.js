exports.config = {
    URL_BASE: process.env.REACT_APP_BASE_URL || '',
    ENV: process.env.NODE_ENV
}