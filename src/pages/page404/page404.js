import React, { Component } from "react"
import { Redirect } from 'react-router-dom'
import { ThemeContext } from "../../context"

const Todo = props => (
  <div style={{
    position: 'absolute',
    top:0,
    left:0,
    bottom:0,
    width: '100%',
    heigth: '100%',
    textAlgin: 'center',
    display:'flex',
    background: 'linear-gradient(to bottom, #6095c9, black)'
  }}>{props.children}</div>
);

const Ingreso = props => (
  <div style={{
    width: '100%',
    border: 'none',
    padding: '6%',
    textAlgin: 'center',
    margin: props.up ? '' : 'auto',
    color: 'white',
    backgroundColor: 'transparent',
  }}>{props.children}</div>
);

const IngresoSeccion = props => (
  <div style={{
    display: 'block',
    marginBottom: '30px',
    position: 'relative',
    textAlgin: 'center'
  }}>{props.children}</div>
);

const IngresoSeccionTitulo = props => (
  <div style={{
    display: 'flex',
    marginBottom: '30px',
    fontSize: '300%',
    position: 'relative',
    textAlgin: 'center',
    margin:'auto',
    backgroundColor:'transparent',
  }}>{props.children}</div>
);

const BotonPrimary = props=>(
  <button 
    style={{
      borderRadius: '10px',
      border: 'none',
      width: '100%',
      height: '110px',
      fontSize: '250%',
      backgroundColor: props.disabled ? '#B88196' : '#6095c9',
      color: props.disabled ? '#dddddd' : 'white',
      outline: 'none',
      cursor:'pointer',
      marginTop:'1%',
      WebkitTapHighlightColor: 'transparent',
    }} 
    onClick={props.onClick}
  >{props.children}</button>
)

const PaginaNoEncontrada= props => (
  <div 
    style={{
        color: 'white',
        margin: 'auto',
        textAlign: 'center',
        fontSize: '130%',
        cursor: 'pointer',
        marginBottom: '10%',
    }}
    onClick={props.onClick}
  >{props.children}</div>
);

class Page404 extends Component { 
  static contextType = ThemeContext
  constructor(props) {
    super(props);
    this.state = {
      redirigir: false,

    };
  }

  renderRedirect = () => {
    if (this.state.redirigir) {
      return <Redirect to='/' />
    }
  }

  render(){
    return (
      <Todo>
        {this.renderRedirect()}
        <Ingreso>

          <IngresoSeccionTitulo>
            <PaginaNoEncontrada>404 - No se encontro la página</PaginaNoEncontrada>
          </IngresoSeccionTitulo>

          <IngresoSeccion>
            <BotonPrimary onClick={()=> this.setState({redirigir: true}) }>IR AL INCIO</BotonPrimary>
          </IngresoSeccion>

        </Ingreso>
      </Todo>
    )
  }
}

export default Page404;