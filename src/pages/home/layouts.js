import React from "react"

const WholePage = props => (
    <div style={{
        width:'100%',
        height: '100%',
        position: 'fixed',
        right: '0%',
        top: '0%',
    }}>{props.children}</div>
)

const MenuLayout = props => (
    <div style={{
        width:'100%',
        height: '5%',
    }}>{props.children}</div>
)

const ContentLayout = props => (
    <div style={{
        position:'absolute',
        width:'100%',
        height: '95%',
        top: '5%',
    }}>{props.children}</div>
)

const LeftLayout = props => (
    <div style={{
        position:'absolute',
        width:'70%',
        height: '100%',
        top:'0%',
        display: 'inline-block',

    }}>{props.children}</div>
)

const RightLayout = props => (
    <div style={{
        position:'absolute',
        width:'30%',
        height: '100%',
        top:'0%',
        left: '70%',
        display: 'inline-block',
    }}>{props.children}</div>
)

const InnerLeftLayout = props => (
    <div style={{
        position:'absolute',
        width:`calc(100% - ${document.getElementById("floatingMenu").offsetWidth}px - 40px - 20px)`,
        height: '100%',
        left:`calc(${document.getElementById("floatingMenu").offsetWidth}px + 30px)`,
        display: 'inline-block',
        paddingLeft: "20px", 
        paddingRight: "20px",
        ...props.style
    }}>{props.children}</div>
)

const TopLeftLayout = props => (
    <div style={{
        height: '25%',
        top:'0%',
        display: 'inline-block',
        width:'100%',
        ...props.style
    }}>{props.children}</div>
)

const BottomLeftLayout = props => (
    <div style={{
        height: '75%',
        top:'25%',
        display: 'inline-block',
        width:'100%',
        ...props.style
    }}>{props.children}</div>
)

export { WholePage, MenuLayout, ContentLayout, LeftLayout, InnerLeftLayout, TopLeftLayout, BottomLeftLayout, RightLayout }