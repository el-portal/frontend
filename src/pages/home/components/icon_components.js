import React, { Component } from 'react'
import { ThemeContext } from "../../../context"
import ExploreIcon from '@material-ui/icons/Explore';
import EmojiPeopleIcon from '@material-ui/icons/EmojiPeople'
import FlagIcon from '@material-ui/icons/Flag';
import ScheduleIcon from '@material-ui/icons/Schedule';
import SearchIcon from '@material-ui/icons/Search';
import SettingsIcon from '@material-ui/icons/Settings';
import { ReactComponent as WorldSVGIcon } from '../icons/world-menu-icon-48x48.svg';
import { ReactComponent as SocietySVGIcon } from '../icons/citizens-icon-50x50.svg' 
import { ReactComponent as ObjectSVGIcon } from '../icons/sword-icon-256x256.svg';
import { ReactComponent as BoardSVGIcon } from '../icons/scroll-message-icon-50x50.svg';
import { ReactComponent as NewsSVGIcon } from '../icons/news-icon-32x32.svg';
import { ReactComponent as DiscordSVGIcon } from '../icons/discord-icon-50x50.svg';
import { ReactComponent as GitLabColorSVGIcon } from '../icons/gitlab-color-icon-128x128.svg';
import SvgIcon from '@material-ui/core/SvgIcon';

const WorldIcon = (props) => ( <SvgIcon component={WorldSVGIcon} viewBox='0 0 48 48' {...props}/> )
const MapIcon = ExploreIcon;
const BoardIcon = (props) => ( <SvgIcon component={BoardSVGIcon} viewBox='0 0 50 50' {...props}/> )
const NewsIcon = (props) => ( <SvgIcon component={NewsSVGIcon} viewBox='0 0 32 32' {...props}/> )

const EventIcon = ScheduleIcon;
const LocationIcon = FlagIcon;
const IndividualIcon = EmojiPeopleIcon;
const SocietyIcon = (props) => ( <SvgIcon component={SocietySVGIcon} viewBox='0 0 50 50' {...props}/> )
const ObjectIcon = (props) => ( <SvgIcon component={ObjectSVGIcon} viewBox='0 0 256 256' {...props}/> )

const GitLabColorIcon = (props) => ( <SvgIcon component={GitLabColorSVGIcon} viewBox='0 0 128 128' {...props}/> )
const DiscordColorIcon = (props) => ( <SvgIcon component={DiscordSVGIcon} viewBox='0 0 50 50' {...props}/> )

export { EventIcon, LocationIcon, IndividualIcon, SocietyIcon, ObjectIcon }
export { WorldIcon, MapIcon, BoardIcon, SearchIcon, SettingsIcon }
export { NewsIcon, GitLabColorIcon, DiscordColorIcon }