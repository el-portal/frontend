import React, { Component } from 'react'
import { EventIcon, LocationIcon, IndividualIcon, SocietyIcon, ObjectIcon } from "../components/icon_components"
import DeleteIcon from '@material-ui/icons/Delete';

const assignIcon = (entryClass, fontSize=undefined, style=undefined) => {
    switch(entryClass){
        case 'events': return (<EventIcon fontSize={fontSize} style={style}/>);
        case 'locations': return (<LocationIcon fontSize={fontSize} style={style}/>)
        case 'individuals': return (<IndividualIcon fontSize={fontSize} style={style}/>)
        case 'societies': return (<SocietyIcon fontSize={fontSize} style={style}/>)
        case 'objects': return (<ObjectIcon fontSize={fontSize} style={style}/>)
        default: return (<DeleteIcon fontSize={fontSize} style={style}/>)
    }
}

const toFancyEntryClass = (entryClass) => {
    switch(entryClass){
        case 'events': return ("Evento")
        case 'locations': return ("Locación")
        case 'individuals': return ("Individuo")
        case 'societies': return ("Sociedad")
        case 'objects': return ("Objeto")
        default: return ("Entrada de Mundo")
    }
}

const toSingEntryClass = (entryClass) => {
    switch(entryClass){
        case 'events': return ("event")
        case 'locations': return ("location")
        case 'individuals': return ("individual")
        case 'societies': return ("society")
        case 'objects': return ("object")
        default: return ("error")
    }
}

const toGenreEntryClass = (entryClass) => {
    let isMasculine = false
    switch(entryClass){
        case 'events': isMasculine = true; break
        case 'locations': isMasculine = false; break
        case 'individuals': isMasculine = true; break
        case 'societies': isMasculine = false; break
        case 'objects': isMasculine = true; break
        default: isMasculine = false; break
    }
    return ( isMasculine )
}

const toFancyNewsClass = (entryClass) => {
    switch(entryClass){
        case 'diffusion': return ("Difusión")
        case 'testimonies': return ("Testimonio")
        default: return ("Entrada de Noticias")
    }
}

const default_events = ["Bilbo roba el Anillo a Gollum", "Frodo obtiene el Anillo", "Caída de Moria", "Muerte de Boromir"];
const default_locations = ["Gondor", "Minas Tirith", "Moria", "Mordor"];
const default_individuals = ["Frodo", "Sam", "Pippin", "Merry"];
const default_societies = ["Comunidad del Anillo", "Nazgûl"]
const default_objects = ["El Anillo Único", "Andúril, la Flama del Oeste", "Poción de curación común"]
const default_entries = [
    {entryClass: "Locaciones", data: "Gondor"},
    {entryClass: "Locaciones", data: "Minas Tirith"},
    {entryClass: "Locaciones", data: "Moria"},
    {entryClass: "Locaciones", data: "Mordor"},
    {entryClass: "Individuos", data: "Frodo"},
    {entryClass: "Individuos", data: "Sam"},
    {entryClass: "Individuos", data: "Pippin"},
    {entryClass: "Individuos", data: "Merry"},
    {entryClass: "Sociedades", data: "Comunidad del Anillo"},
    {entryClass: "Sociedades", data: "Nazgûl"},
    {entryClass: "Objetos", data: "El Anillo Único"},
    {entryClass: "Objetos", data: "Andúril, la Flama del Oeste"},
    {entryClass: "Objetos", data: "Poción de curación común"},
]

const display_event = {
    name:'Frodo obtiene el anillo',
    publicDescription:'Habiéndose marchado Bilbo, Frodo se encuentra en Bolsón Cerrado con Gandalf y con una carta que contiene un anillo muy singular.',
    impact:'Alto',
    happenedIn:['La Comarca'],
    keyInformation:'Frodo obtiene el Anillo Único',
    longDescription:'En una extraña despedida, Bilbo se marcha de repente desapareciendo de su cumpleaños. Frodo vuelve a Bolsón Cerrado y allí se encuentra con un Gandalf de aspecto preocupado. Le dice que Bilbo le ha dejado una carta y en ella encuentra un anillo muy singular. Instado por el mago, lo arroja al fuego y se rebela una tétrica inscripción. Afectado, Gandalf decide ir a buscar más información, descubre a Sam espiando la conversación y les encarga a ambos irse velozmente de La Comarca.',
    involvedIndividuals: ['Frodo Bolsón', 'Sam Samsagaz Gamyi'],
    involvedSocieties: [],
    involvedObjects: ['El Anillo Único'],
    changes:'Frodo ahora posee el Anillo Único',
    entryClass:'events'
}

const display_location = {
    name:'La Comarca',
    publicDescription:'Región de colinas verdes y campos vivarachos en las que viven los medianos dentro de casas de techos bajos y puertas y ventanas redondas.',
    locationType: 'Región',
    fame: 'Baja',
    danger: 'Bajo',
    longDescription: 'Tierras verdes y pacíficas, que incluyen colinas, llanuras, campos y bosques de los que los medianos están enamorados. Se divide políticamente en cuatro cuadernas, siendo la Cuaderna del Oeste más habitada de todas. Es allí donde se sitúa Hobbiton.\nAunque los medianos no lo saben y viven con poco interés en el exterior, los hombres Montaraces protegen estas tierras de posibles ataques enemigos.',
    relatedIndividuals: ["Frodo Bolsón", "Sam Samsagaz Gamyi", "Pippin Peregrin Tuk", "Merry Meriadoc Brandigamo"],
    relatedLocations: [],
    notableTreasure: [],
    entryClass: 'locations',
}

const display_other_location = {
    name: 'Mordor',
    publicDescription: 'Región desolada y rodeada de montañas donde Sauron, el Señor Oscuro, edificó su fortaleza Barad-dûr para atacar y dominar la Tierra Media.',
    locationType: 'Región',
    fame: 'Media',
    danger: 'Bajo',
    longDescription: 'Región desolada y plana, con un interior desértico situado a gran altitud y rodeado de una muralla natural formada por Ered Lithui, las Montañas de Ceniza, y Ephel Duáth, las Montañas de la Sombra. En su interior, el Señor Oscuro Sauron edificó su fortaleza Barad-dûr, la Torre Oscura, y desde allí vigila la Tierra Media, a la que aún quiere dominar. Dentro de Mordor se halla el gigantesco volcán llamado el Monte del Destino donde el Anillo Único fue forjado. Sólo allí puede ser destruido.',
    relatedIndividuals: [],
    relatedLocations: [],
    notableTreasure: [],
    entryClass: 'locations'
}

const display_individual = {
    name:'Frodo Bolsón',
    publicDescription:'Mediano amable y callado de ojos azules y piel clara, sobrino de Bilbo Bolsón y portador del Anillo Único y del destino de la Tierra Media.',
    race:'Mediano/a',
    occupation:'Aventurero/a',
    influence:'Alta',
    danger:'Bajo',
    longDescription:'Frodo es un hobbit de ojos azules claros y que tiene una piel blanca y pálida. Es algo diferente a los demás: no come demasiado, le gusta leer y es más sensible que los demás, siempre abierto a escuchar noticias de afuera de la Comarca a través del amigo de su tío, Gandalf el Gris. Actualmente es el portador del Anillo Único, legado por Bilbo Bolsón. Forma parte de la Comunidad del Anillo y debe llevarlo en persona hasta el terrible Monte del Destino y destruirlo en los fuegos que lo forjaron.',
    usualLocations:['La Comarca'],
    relatedIndividuals:["Sam Samsagaz Gamyi", "Merry Meriadoc Brandigamo", "Pippin Peregrin Tuk"],
    associatedSocieties: ["La Comunidad del Anillo"],
    notableInventory: ["El Anillo Único"],
    entryClass:'individuals'
}

const display_society = {
    name:'Comunidad del Anillo',
    publicDescription:'Grupo de nueve aventureros de razas variadas cuya misión es escoltar a Frodo Bolsón y el Anillo Único al Monte del Destino para destruirlo.',
    societyType:'Grupo de aventurero/as',
    danger:'Medio',
    influence:'Alta',
    members:'Baja',
    secretism:'Alto',
    longDescription:'También conocida como la Compañía del Anillo, esta asociación fue creada en Rivendel durante el Concilio de Elrond. Buscando una solución al inminente retorno de Sauron, representantes de varias razas y naciones acudieron al llamado. Debido a antiguas enemistades surgieron disputas que sólo se solucionaron cuando Frodo propuso que él llevaría el anillo al Monte del Destino en Mordor para destruirlo. Movidos por su valentía, otros ocho aventureros se unieron a pesar de sus diferencias.',
    relatedLocations:[],
    relatedIndividuals:["Frodo Bolsón", "Sam Samsagaz Gamyi", 
                        "Pippin Peregrin Tuk", "Merry Meriadoc Brandigamo",
                        'Aragorn Trancos Elessar Telcontar'],
    relatedSocieties: [],
    notableTreasure: [],
    entryClass: 'societies',
}

const display_object = {
    name:'El Anillo Único',
    publicDescription:'Poderoso anillo mágico de peligrosa influencia forjado por el Señor Oscuro Sauron para controlar al resto y esclavizar la Tierra Media.',
    influence:'Alta',
    danger:'Alto',
    objectType:'Anillo',
    magic:true,
    magicRarity:'Legendario',
    longDescription:'Anillo de oro que se ajustaba al dedo del portador, con un grabado en la Lengua Negra, el cual decía: "Un Anillo para gobernarlos a todos, un Anillo para encontrarlos, un Anillo para atraerlos a todos y en las tinieblas atarlos".\n\nFue creado por Sauron en el Monte del Destino después de engañar a los herreros elfos de Eregion para que le enseñaran el arte de forjar anillos. Su intención era dominar con él a todos los otros Anillos de Poder y esclavizar así a los Pueblos Libres de la Tierra Media.',
    possibleLocations:[],
    possibleIndividualOwners:['Frodo Bolsón', "Sam Samsagaz Gamyi"],
    possibleSocietyOwners:['Comunidad del Anillo'],
    created_ts: Date.now() -  Math.floor(Math.random() * 100),
}

export { assignIcon, toFancyEntryClass, toSingEntryClass, toGenreEntryClass, toFancyNewsClass }
export { default_events, default_locations, default_individuals, default_societies, default_objects, default_entries }
export { display_event, display_location, display_individual, display_society, display_object, display_other_location }