import React, { Component } from 'react'
import { NewsIcon, BoardIcon } from "./icon_components"
import { ChipField } from "./show_components"
import DeleteIcon from '@material-ui/icons/Delete';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import { default_entries } from './world_entry_consts';
import { Typography } from '@material-ui/core';

const dateOptions = {
  // timeStyle:'medium',
  year:'2-digit',
  month:'2-digit',
  day:'2-digit',
  hour:'2-digit',
  minute:'2-digit',
  second:'2-digit',
  weekday:'short',
  // timeZoneName:'short'
}

const toDateFormatted = (created_ts) => {
  let dateString = new Intl.DateTimeFormat('es-AR', dateOptions).format(new Date(created_ts));
  dateString = ( dateString.charAt(0).toUpperCase() + dateString.slice(1) ).split('., ').join(' ')
  return (dateString)
}


const assignNewsIcon = (newsClass, fontSize=undefined, style=undefined) => {
  switch(newsClass){
    case 'diffusion': return (<NewsIcon fontSize={fontSize} style={style}/>)
    case 'testimonies': return (<BoardIcon fontSize={fontSize} style={style}/>)
    default: return (<DeleteIcon fontSize={fontSize} style={style}/>)
  }
}

const assignNewsTitle = (newsClass, newsEntry) => {
  switch(newsClass){
    case 'diffusion': return (newsEntry.diffusionType);
    case 'testimonies': 
      if (newsEntry.isIndividual) { return (`${newsEntry.witnessIndividual.name}:`) }
      else { return (`${newsEntry.witness}:`) }
    default: return ("Nadie:")
  }
}

const NewsListItem = props => {
  const onClick = () => { props.changePage('testimonies', props.entry) }
  return(
    <ListItem button key={props.key} onClick={onClick}>
      <ListItemIcon>{assignNewsIcon(props.newsClass)}</ListItemIcon>
      <ListItemText 
        primary={assignNewsTitle(props.newsClass, props.newsEntry)} 
        secondary={props.newsEntry.content}
        secondaryTypographyProps={{variant:'inherit', fontStyle:"italic"}} />
    </ListItem>
  )
}

class ShowHideWrapper extends Component {
  render(){
    if (this.props.show) { return ( this.props.children )
    } else { return ( <div/> ) }
  }
}

class BinaryOscillatingWrapper extends Component {
  render(){
    return ( this.props.children[this.props.switch ? 1 : 0] )
  }
}

class TextIdOscillatingWrapper extends Component {
  idsList = this.props.children.map( (child) => ( child.props.id )  )
  render(){
    return ( this.props.children[ this.idsList.findIndex( (id) => (id===this.props.switch) ) ] )
  }
}

const NewsTextField = (props) => {
  const onClick = () => { props.changePage('news', props.entryId ) }

  console.log(props.diffusion)
  console.log(typeof props.diffusion)
  console.log(props.diffusion !== [])
  let newsText = ''
  try {
    if (props.diffusion !== [] && props.diffusion.length > 0 && typeof props.diffusion !== "undefined") {
      newsText = `1 ${props.diffusion[0].diffusionType.toLowerCase()}`
    }
  } catch {}
  try {
    if (props.testimonies !== [] && props.testimonies.length > 0 && typeof props.testimonies !== "undefined") {
      if ( newsText != "" ) { newsText = newsText + " y " }
      if ( props.testimonies.length > 1 ) { newsText = newsText + props.testimonies.length + " testimonios"
      } else { newsText = newsText + props.testimonies.length + " testimonio" }
      
    }
  } catch {}

  if (newsText !== "") { return (
    <div>
      <Typography style={props.style}> <b>Noticias:</b> </Typography>
      <div style={{...props.style, display: 'inline-block', padding:'2px'}}>
        <ChipField
          id={props.id}
          label={newsText}
          onClick={onClick}
        />
      </div>
    </div>
  ) } else { return (
    <div>
      <Typography style={props.style}> <b>Noticias:</b>  </Typography>
      <div style={{...props.style, display: 'inline-block', padding:'2px'}}>
        <ChipField
            id={props.id}
            label={"¿Crear difusión?"}
            onClick={onClick}
          />
      </div>
    </div>
  ) }
}

export { ShowHideWrapper, BinaryOscillatingWrapper, TextIdOscillatingWrapper }
export { toDateFormatted, NewsListItem, NewsTextField }
