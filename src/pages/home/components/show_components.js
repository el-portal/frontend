import React, { Component } from 'react'
import { ThemeContext } from "../../../context"
import DeleteIcon from '@material-ui/icons/Delete';
import { Typography } from '@material-ui/core';
import Chip from '@material-ui/core/Chip';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import { assignIcon, toSingEntryClass, toGenreEntryClass, display_other_location } from "./world_entry_consts";

const toNoneFromGenre = (isMasculine) => {
    if (isMasculine) { return("Ninguno") } else { return("Ninguna") }
}

const NamePublicDescriptionField = props => (
    <h1 style={props.style}> 
        {assignIcon(props.entryClass, 'large')}
        {` ${props.name}`}

        <Typography style={{paddingTop:'1em'}}>
            <i>{props.publicDescription}</i>
        </Typography>
    </h1>
)

const OneLineField = props => {
    if (props.value!=="") { return (
        <Typography style={props.style}>
            <b>{props.label}:</b> {props.value}
        </Typography>
    )} else { return (
        <Typography style={props.style}>
            <b>{props.label}:</b> {toNoneFromGenre(props.isMasculine)}
        </Typography>
    )}
}

const ParagraphField = props => {
    if (props.value!=="") { return (
        <div style={props.style}>
            <Typography> <b>{props.label}:</b> </Typography>
            <Typography style={{paddingTop:'1em'}}> {props.value} </Typography>
        </div>
    )} else { return (
        <div style={props.style}>
            <Typography> <b>{props.label}:</b> </Typography>
            <Typography style={{paddingTop:'1em', paddingLeft:'1em'}}> <i>{toNoneFromGenre(props.isMasculine)}</i> </Typography>
        </div>
    )}
}

const TrueFalseField = props => {
    let yesNo
    if (props.value) {
        yesNo = "Sí"
    } else {
        yesNo = "No"
    }
    return (
        <Typography style={props.style}>
            <b>{props.label}:</b> {yesNo}
        </Typography>
    )
}

const ChipField = props => {
    if (props.deletable===true) { return (
        <Chip 
            id={props.id}
            avatar={props.avatar}
            icon={props.icon}
            label={props.label}
            disabled={props.disabled}
            clickable={props.clickable}
            onClick={props.onClick}
            deleteIcon={<DeleteIcon/>}
            onDelete={props.onDelete}
            syle={props.style}
        />
    )} else { return (
        <Chip 
            id={props.id}
            avatar={props.avatar}
            icon={props.icon}
            disabled={props.disabled}
            label={props.label}
            clickable={props.clickable}
            onClick={props.onClick}
            syle={props.style}
        />
    )}
}

const EntryChipField = props => {
    const onClick = () => { props.changePage('show-' + toSingEntryClass(props.entry.entryClass), props.entry._id ) }
    return(
        <div style={{...props.style, display: 'inline-block', padding:'2px'}}>
            <ChipField 
                id={props.id}
                icon={assignIcon(props.entry.entryClass)}
                label={props.entry.name}
                disabled={props.disabled}
                clickable
                onClick={onClick}
                deletable={props.deletable}
                onDelete={props.onDelete}
            />
        </div>
    )
}

// Attention: This is made out of a list of reduced entries. They must be searched later on by id.
// Example: {'_id': '60a947772fb21cb232d5aeed', 'name': 'Frodo', 'Mediano que lleva el Anillo', publicDescription: "Blah", entryClass:'individuals'}
const EntryChipListField = props => {
    let entryList
    if( props.entryList === null ) { 
        return ( <div/> )
    } else if ( Array.isArray(props.entryList)) {
        entryList = props.entryList
    } else {
        entryList = [props.entryList]
    }
    if (entryList.length>0 && entryList[0]!==""){
        return (
            <div style={props.style}>
                <Typography> <b>{props.label}:</b> </Typography>
                <div style={{paddingTop:'1em'}}>
                    {entryList.map((entry, index) => {
                        return (
                            <EntryChipField id={"ec"+index}
                                entry={entry} 
                                changePage={props.changePage}
                            />
                        )
                    })}
                </div>
            </div>
        )
    } else {
        return (
            <div style={{width:'100%', ...props.style}}>
                <Typography> <b>{props.label}:</b> </Typography>
                <Typography style={{paddingTop:'1em', paddingLeft:'1em'}}> <i>{toNoneFromGenre(toGenreEntryClass(props.entryClass))}</i></Typography>
            </div>
        )
    }
}

const EntryListItem = props => {
    const onClick = () => { props.changePage('show-' + toSingEntryClass(props.entry.entryClass), props.entry._id ) }
    return(
        <ListItem button key={props.key} onClick={onClick}>
            <ListItemIcon>{assignIcon(props.entry.entryClass)}</ListItemIcon>
            <ListItemText primary={props.entry.name} secondary={props.entry.publicDescription} />
        </ListItem>
    )
}

// Attention: This is also made out of a list of reduced entries. They must be searched later on by id.
// Example: {'_id': '60a947772fb21cb232d5aeed', 'name': 'Frodo', 'Mediano que lleva el Anillo', publicDescription: "Blah", entryClass:'individuals'}
const EntryListField = props => {
    let entryList
    if( props.entryList === null ) { 
        return ( <div/> )
    } else if ( Array.isArray(props.entryList)) {
        entryList = props.entryList
    } else {
        entryList = [props.entryList]
    }
    if (entryList.length>0 && entryList[0]!=="") {
        return (
            <List style={props.style}>
            {entryList.map((entry, index) => {
                return (
                    <EntryListItem key={index}
                        entry={entry}
                        changePage={props.changePage}
                    />
            )})}
            </List>
        )
    } else {
        return (
            <div style={{width:'100%', ...props.style}}>
                <Typography style={{paddingTop:'1em', paddingLeft:'1em'}}> 
                    <i>{props.errorLabel}</i>
                </Typography>
            </div>
        )
    }
}

export { NamePublicDescriptionField, OneLineField, ParagraphField, TrueFalseField, 
         ChipField, EntryChipField, EntryChipListField, EntryListItem, EntryListField }