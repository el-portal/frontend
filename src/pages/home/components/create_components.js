import React, { Component } from 'react'
import { ThemeContext } from "../../../context"
import TextField  from '@material-ui/core/TextField'; 
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
// import FormLabel from '@material-ui/core/FormLabel';
// import FormHelperText from '@material-ui/core/FormHelperText';
import Checkbox from '@material-ui/core/Checkbox';
import Switch from '@material-ui/core/Switch';
import MenuItem from '@material-ui/core/MenuItem';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { assignIcon, toFancyEntryClass } from "./world_entry_consts";

const maxNameTextField = 50 // Almost 1/6 Tweet
const maxShortTextField = 140 // 1/2 Tweet
const maxMidTextField = 280 // Tweet
const maxLongTextField = 500 // Almost 2 Tweets

const default_options = ["Frodo", "Sam", "Pippin", "Merry"];

const default_dict_options = [
    { key: 'The Shawshank Redemption', year: 1994 },
    { key: 'The Godfather', year: 1972 },
    { key: 'The Dark Knight', year: 2008 },
    { key: '12 Angry Men', year: 1957 },
    { key: "Schindler's List", year: 1993 },
    { key: 'Pulp Fiction', year: 1994 },
    { key: 'The Lord of the Rings: The Return of the King', year: 2003 },
    { key: 'The Good, the Bad and the Ugly', year: 1966 },
];

const listToArrayDict = (listOptions) => {
    var arrayDictOptions = []
    for (var opt of listOptions) {
        arrayDictOptions.push({"key": opt})
    }
    return arrayDictOptions
}

const arrayDictToList = (arrayDictOptions) => {
    var listOptions = []
    for (var optDict of arrayDictOptions) {
        listOptions.push(optDict["key"])
    }
    return listOptions
}

const listToLowerCase = (stringList) => {
    var stringListLower = []
    for (var aString of stringList) {
        stringListLower.push(aString.toLowerCase())
    }
    return stringListLower
}

const chooseGenre = (isMasculine, optionsMasculineFemenine) => {
    if (isMasculine) {
        return optionsMasculineFemenine[0]
    } else {
        return optionsMasculineFemenine[1]
    }
}

const maxTextHelp = (value, maximum) => {
    if (value.length >= 1) {
        return `${value.length} de ${maximum} caracteres`
    } else {
        return
    }
}

const TitleField = props => (
    <h1 style={props.style}> 
        Crear {`${toFancyEntryClass(props.entryClass)} `}
        {assignIcon(props.entryClass, "large", {marginLeft:".1em"})}
    </h1>
)

const NameField = props => (
    <TextField 
        id={props.id}
        label={props.label}
        InputLabelProps={{ style: { fontSize: '130%' } }}
        helperText={maxTextHelp(props.value, maxNameTextField)}
        error={props.value.length === maxNameTextField}
        variant="outlined"
        inputProps={{ maxLength: maxNameTextField }}
        InputProps={{ style: {fontWeight: 'bold', fontSize: '150%'} }}
        style={props.style} //display:'block'
        onChange={props.onChange}
        value={props.value}
        disabled={props.disabled}
    />
)

const SearchField = props => (
    <TextField 
        id={props.id}
        label={props.label}
        helperText={maxTextHelp(props.value, maxNameTextField)}
        error={props.value.length === maxNameTextField}
        variant="outlined" 
        multiline
        inputProps={{ maxLength: maxNameTextField }}
        style={props.style} 
        onChange={props.onChange}
        value={props.value}
        disabled={props.disabled}
    />
)

const TinyDescriptionField = props => (
    <TextField 
        id={props.id}
        label={props.label}
        helperText={maxTextHelp(props.value, maxNameTextField)}
        error={props.value.length === maxNameTextField}
        variant="outlined"
        inputProps={{ maxLength: maxNameTextField }}
        style={props.style} //display:'block'
        onChange={props.onChange}
        value={props.value}
        disabled={props.disabled}
    />
)

const ShortDescriptionField = props => (
    <TextField 
        id={props.id}
        label={props.label}
        helperText={maxTextHelp(props.value, maxShortTextField)}
        error={props.value.length === maxShortTextField}
        variant="outlined" 
        multiline
        inputProps={{ maxLength: maxShortTextField }}
        style={props.style} 
        onChange={props.onChange}
        value={props.value}
        disabled={props.disabled}
    />
)

const MidDescriptionField = props => (
    <TextField 
        id={props.id}
        label={props.label}
        helperText={maxTextHelp(props.value, maxMidTextField)}
        error={props.value.length === maxMidTextField}
        variant="outlined" 
        multiline
        inputProps={{ maxLength: maxMidTextField }}
        style={props.style} 
        onChange={props.onChange}
        value={props.value}
        disabled={props.disabled}
    />
)

const LongDescriptionField = props => (
    <TextField 
        id={props.id}
        label={props.label}
        helperText={maxTextHelp(props.value, maxLongTextField)}
        error={props.value.length === maxLongTextField}
        variant="outlined" 
        multiline
        inputProps={{ maxLength: maxLongTextField }}
        style={props.style} 
        onChange={props.onChange}
        value={props.value}
        disabled={props.disabled}
    />
)

const BinaryField = props => (
    <FormControl component="fieldset" style={{...props.style, verticalAlign:'center', height:'56px'}}>
    {/* <div style={{...props.style, display:'flex', justifyContent:'flex-start', alignItems:'center', height:'56px'}}> */}
        <FormControlLabel
            label={props.label}
            style={{display:'block', padding:'auto'}}
            control={
                <Switch 
                    id={props.id}
                    checked={props.value} 
                    onChange={props.onChange}
                    style={{display:'block', margin:'auto'}}
                />
            }
        />
    {/* </div> */}
    </FormControl>
)

const CheckboxField = props => (
    <FormControl component="fieldset" style={{...props.style}}>
    {/* <FormLabel component="legend">Filtrar por...</FormLabel> */}
    <FormGroup row={props.row}>
        {props.name.map((n, i) => {
            return <FormControlLabel 
                control={<Checkbox 
                    checked={props.checked[i]} 
                    color="default" 
                    onChange={props.onChange[i]} 
                    name={n}/>}
                label={props.label[i]}
            />
        })}
    </FormGroup>
    {/* <FormHelperText>Be careful</FormHelperText> */}
  </FormControl>
)

const SelectField = props => (
    <FormControl id={`${props.id}-form-control`} variant="outlined" style={props.style} disabled={props.disabled}>
    <InputLabel id={`${props.id}-input-label`}>{props.label}</InputLabel>
    <Select 
        id={`${props.id}-select`}
        label={props.label}
        labelId={`${props.id}-label-id`}
        onChange={props.onChange}
        value={props.value}
    >
        {props.selectValues.map((svalue, index) => {
            return <MenuItem id={`${props.id}-${svalue}`} value={svalue} key={svalue.toLowerCase()}>{svalue}</MenuItem>
        })}
    </Select>
    </FormControl>
)

const HighMediumLowField = props => (
    <SelectField
        id={props.id}
        label={props.label}
        inputlabel={props.label}
        selectValues={chooseGenre(props.isMasculine, [["Alto", "Medio", "Bajo"], ["Alta", "Media", "Baja"]])}
        style={props.style}
        onChange={props.onChange}
        value={props.value}
        disabled={props.disabled}
    />
)

const TagsField = props => (
    <FormControl component="fieldset" style={{...props.style}}>
        <Autocomplete
            id={props.id}
            multiple
            options={props.options}
            groupBy={props.groupBy}
            getOptionLabel={props.getOptionLabel}
            // getOptionLabel={(option) => option}
            // getOptionLabel={(option) => option.key}
            onChange={props.onChange}
            value={props.value}
            disabled={props.disabled}
            renderInput={(params) => (
                <TextField {...params} variant="outlined" label={props.label}  />
            )}
        />
    </FormControl>
)

const SuggestSelectField = props => (
    <FormControl component="fieldset" style={{...props.style}}>
        <Autocomplete
            id={props.id}
            freeSolo={props.allowNew}
            disableClearable
            autoSelect
            clearOnEscape
            options={props.options}
            groupBy={props.groupBy}
            getOptionLabel={props.getOptionLabel}
            // getOptionLabel={(option) => option}
            // getOptionLabel={(option) => option.key}
            onChange={props.onChange}
            value={props.value}
            disabled={props.disabled}
            renderInput={(params) => (
                <TextField
                    {...params}
                    label={props.label}
                    variant="outlined"
                    InputProps={{ ...params.InputProps, type: 'search' }}
                />
            )}
        />
    </FormControl>
)

export { 
    TitleField, NameField, SearchField, TinyDescriptionField, ShortDescriptionField, MidDescriptionField, LongDescriptionField, 
    BinaryField, CheckboxField, SelectField, HighMediumLowField, TagsField, SuggestSelectField 
}
export { default_dict_options, default_options, listToArrayDict, arrayDictToList , listToLowerCase }