const headerStyle = { padding:'10px 0px 0px 0px', textAlign:'left' }
const scrollStyle = { height:'100%', overflow:'scroll', overflowX:'hidden', overflowY:'auto' }

const divFlexStyle = { display:'inline-block', paddingLeft:'20px', paddingRight:'20px', width:'calc(100%-40px)' }
// const divFlexStyle = { display:'flex', flexDirection:'row', alignItems:'flex-start', justifyContent:'flex-start', marginLeft:'20px', flexWrap:'wrap' }
const rowFlexStyle = { width:'100%', margin:'10px 0px 10px 0px', }
// const rowFlexStyle = { width:'calc(100% - 20px)', flexBasis:'auto', alignmargin:'10px 0px 10px 0px'}
const colorGreen = { color: "black", background: 'linear-gradient(45deg, #44FE80 30%, #90FEB3 90%)' }

const divSearchRowFlexStyle = { width:'100%', display:'flex', flexDirection:'row', alignItems:'flex-start', justifyContent:'space-between', marginTop:'20px', flexWrap:'wrap' }
const divSearchColFlexStyle = { display:'flex', flexDirection:'column', alignItems:'flex-start', justifyContent:'flex-start', marginTop:'20px', flexWrap:'wrap' }
const rowButtonFlexStyle = { width:'calc(100% - 20px)', flexBasis:'auto', alignmargin:'10px 0px 10px 0px'}
const colSearchFlexStyle = { width:'calc(100% - 20px)', height:'calc(100% - 20px)', flexBasis:'auto', alignmargin:'0px 10px 0px 10px'}

let numberOfItems = 2
let percentWidth = 100/numberOfItems
let extraMargin = 10*(numberOfItems-1)
const rowSym2FlexStyle = [
  { width:`calc(${percentWidth}% - ${extraMargin}px)`, margin:'10px 10px 10px 0px' },
  { width:`calc(${percentWidth}% - ${extraMargin}px)`, margin:'10px 0px 10px 10px' }
]
const rowASym2FlexStyle = [
    { width:`calc(30% - ${extraMargin}px)`, margin:'10px 10px 10px 0px' },
    { width:`calc(70% - ${extraMargin}px)`, margin:'10px 0px 10px 10px' }
  ]

const rowASym2TFlexStyle = [
    { width:`calc(70% - ${extraMargin}px)`, margin:'10px 10px 10px 0px' },
    { width:`calc(30% - ${extraMargin}px)`, margin:'10px 0px 10px 10px' }
  ]

numberOfItems = 2
percentWidth = 100/numberOfItems
extraMargin = 10*(numberOfItems-1)
const rowASym3FlexStyle = [
    { width:`calc(22% - ${extraMargin}px)`, margin:'10px 10px 10px 0px' },
    { width:`calc(48% - ${extraMargin}px)`, margin:'10px 5px 10px 5px' },
    { width:`calc(30% - ${extraMargin}px)`, margin:'10px 0px 10px 10px' }
  ]

export { scrollStyle, headerStyle, divFlexStyle, rowFlexStyle, 
         rowSym2FlexStyle, rowASym2FlexStyle, rowASym2TFlexStyle, rowASym3FlexStyle, colorGreen }
export { divSearchColFlexStyle, divSearchRowFlexStyle, rowButtonFlexStyle, colSearchFlexStyle }