import React, { Component } from 'react'
import { WholePage, MenuLayout, ContentLayout, LeftLayout, RightLayout } from './layouts'
import TopMenu  from './top_menu'
import FloatingMenu from './floating_menu'
import Configuration from './subpages/configuration'
import Map from './subpages/map'
import Search from './subpages/search'
import EntriesBoard from './subpages/board/entries_board'
import { NewsBoard } from './subpages/board/news_board'
import CreateEvent from './subpages/create/event'
import CreateLocation from './subpages/create/location'
import CreateIndividual from './subpages/create/individual'
import CreateSociety from './subpages/create/society'
import CreateObject from './subpages/create/object'
import ShowEvent from './subpages/show/event'
import ShowLocation from './subpages/show/location'
import ShowIndividual from './subpages/show/individual'
import ShowSociety from './subpages/show/society'
import ShowObject from './subpages/show/object'
import { News } from './subpages/news/news'
import axios from 'axios'
import { config } from "../../config" 
import { consoleLogDebugCreator } from "../../logger"

const verbosityLevel = 2
const consoleLogDebug = consoleLogDebugCreator(verbosityLevel)

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      page: 'home',
      Left : null,
      Right : null,
    }
    this.changePage=this.changePage.bind(this)
    this.changeLayout=this.changeLayout.bind(this)
    this.getEntryById=this.getEntryById.bind(this)
  }

  componentDidMount(){
    console.log('Mounting home')
    if (this.state.Left === null || this.state.Right === null) {
      this.setState({page:"home"})
      this.changeLayout("home")
    }
  }

  async getEntryById( entryId ) {return new Promise(async (resolve, reject) => {
    let entry
    await axios.post( config.URL_BASE + `world_entries/world/get/id` ,  {"entryId":entryId}  ).then( (response) => { // '/links/related'
        if(response.status < 300 ){
            if (typeof response.data === "undefined") { 
                consoleLogDebug(`Warning: undefined en recuperación de entrada con id ${entryId}`);
            } else {
                entry = {...response.data}
                consoleLogDebug(`Recuperación exitosa de entrada con id ${entryId}`);
            }
        } else {
            consoleLogDebug(`Error en recuperación de entrada con id ${entryId}`);
        }
    }).catch( (err) =>{
        consoleLogDebug(`Error al enviar pedido de entrada con id ${entryId}`);
        reject(err);
    })
    resolve (entry) 
})}

  async changePage(newPage, entryId=undefined){
    if (entryId) {
      this.changeLayout("blank")
      console.log(`Changing page to '${newPage}' to show an entry with id '${entryId}'`)
      this.setState({page:newPage})
      const entry = await this.getEntryById(entryId)
      this.changeLayout(newPage, entry)
    } else {
      console.log(`Changing page to '${newPage}'`)
      this.setState({page:newPage})
      this.changeLayout(newPage)
    }
  }

  changeLayout(newPage, entry=null){
    let newRight = this.state.Right
    let newLeft = this.state.Left
    switch(newPage){
      case 'home': 
        newRight = <EntriesBoard changePage={this.changePage}/>
        newLeft = <Map changePage={this.changePage}/>
      break
      case 'map': 
        newLeft = <Map changePage={this.changePage}/>
      break
      case 'board': 
        newRight = <EntriesBoard changePage={this.changePage}/>
      break
      case 'blank': 
        newRight = <div/>
      break
      case 'configuration': 
        newLeft = <Configuration changePage={this.changePage}/>
      break
      case 'create-event':
        newRight = <CreateEvent changePage={this.changePage}/>
      break
      case 'create-location':
        newRight = <CreateLocation changePage={this.changePage}/>
      break
      case 'create-individual':
        newRight = <CreateIndividual changePage={this.changePage}/>
      break
      case 'create-society':
        newRight = <CreateSociety changePage={this.changePage}/>
      break
      case 'create-object':
        newRight = <CreateObject changePage={this.changePage}/>
      break
      case 'show-event':
        newRight = <ShowEvent entry={entry} changePage={this.changePage}/>
      break
      case 'show-location':
        newRight = <ShowLocation entry={entry} changePage={this.changePage}/>
      break
      case 'show-individual':
        newRight = <ShowIndividual entry={entry} changePage={this.changePage}/>
      break
      case 'show-society':
        newRight = <ShowSociety entry={entry} changePage={this.changePage}/>
      break
      case 'show-object':
        newRight = <ShowObject entry={entry} changePage={this.changePage}/>
      break
      case 'news':
        newLeft = <News entry={entry} changePage={this.changePage}/>
        newRight = <ShowEvent entry={entry} changePage={this.changePage}/>
      break
      case 'search':
        newLeft = <Search changePage={this.changePage}/>
      break
      default:
        newRight = <EntriesBoard changePage={this.changePage}/>
        newLeft = <Map changePage={this.changePage}/>
      break
    }
    this.setState({Right:newRight, Left:newLeft})
  }

  logout(){
    console.log('Logging out')
    axios.get('/logout').catch ( err => {
      console.log('Error logging out: ' + err)
    })
  }

  render(){
    return (
      <WholePage>
        <MenuLayout>
          <TopMenu changePage={this.changePage} />
        </MenuLayout>
        <ContentLayout>
          <FloatingMenu changePage={this.changePage}  page={this.state.page}/>
          <LeftLayout>
            {this.state.Left}
          </LeftLayout>
          <RightLayout>
            {this.state.Right}
          </RightLayout>
        </ContentLayout>
      </WholePage>  
    )  
  }

}

export default Home;
