import React from "react"
import { ThemeContext } from "../../context"
import { GitLabColorIcon, DiscordColorIcon } from './components/icon_components'

const FloatingSocialButtons = props => (
    <ThemeContext.Consumer>
        {value => (<div style={{
            width: '10%', 
            height: '5%',
            position: 'absolute',
            right: '12%',
            bottom: '8%',
            backgroundColor: 'transparent',
            color: 'transparent',
            zIndex: '100',
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-around',
        }}>
            <ButtonDiscord changePage={props.changePage}/>
            <ButtonGitLab changePage={props.changePage}/>
        </div>)}
    </ThemeContext.Consumer>
);

const ButtonDiscord = props => (
    <ThemeContext.Consumer>
        {value => (
            <div>
                <a href="https://discord.gg/qeskeFxEZE">
                    <DiscordColorIcon style={{
                            borderRadius: '50%',
                            width: '53px',
                            height: '53px',
                            color: '#4E3DFE',
                            backgroundColor: 'black',
                            padding: '7px',
                            margin: '0px 15px 0px 0px',
                            cursor: 'pointer',
                        }}
                    />
                </a>
            </div>
        )}
    </ThemeContext.Consumer>
);

const ButtonGitLab = props => (
    <ThemeContext.Consumer>
        {value => (
            <div>
                <a href="https://gitlab.com/el-portal">
                    <GitLabColorIcon style={{
                            borderRadius: '50%',
                            width: '53px',
                            height: '53px',
                            padding: '7px',
                            margin: '0px 15px 0px 0px',
                            backgroundColor: 'black',
                            // color: 'transparent',
                            // fontSize: '120%',
                            cursor: 'pointer',
                        }}/>
                </a>
            </div>
        )}
    </ThemeContext.Consumer>
);

export default FloatingSocialButtons