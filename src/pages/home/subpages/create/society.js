import React, { Component } from 'react'
import { consoleLogDebugCreator } from "../../../../logger"
import { ThemeContext } from "../../../../context"
import { scrollStyle, headerStyle, divFlexStyle, rowFlexStyle, rowSym2FlexStyle, colorGreen } from "../../style"
import { getReducedOptionLabel, getReducedOptions, getSimpleOptions, checkEntry, sendEntry } from './create_methods'
import { TitleField, NameField, ShortDescriptionField, LongDescriptionField,
         HighMediumLowField, TagsField, SuggestSelectField } from "../../components/create_components"
import { toFancyEntryClass, default_locations, default_individuals, default_societies, default_objects } from "../../components/world_entry_consts"
import Button from '@material-ui/core/Button';

const verbosityLevel = 0;
const consoleLogDebug = consoleLogDebugCreator(verbosityLevel);

const default_society_types = ["Nacionalidad", "Religión", "Sociedad Secreta", "Culto", "Parlamento"]

const default_empty_society = {
  name:'',
  publicDescription:'',
  societyType:'',
  danger:'',
  influence:'',
  members:'',
  secretism:'',
  longDescription:'',
  relatedLocations:[],
  associatedIndividuals:[],
  relatedSocieties: [],
  notableTreasure: [],
}

const default_options_societies = {
  societyTypeOptions: default_society_types,
  relatedLocationsOptions: default_locations,
  associatedIndividualsOptions: default_individuals,
  relatedSocietiesOptions: default_societies,
  notableTreasureOptions: default_objects,
}

const required_fields_societies = ["name", "publicDescription", "societyType", "danger", "influence", "members", "secretism"]

class CreateSociety extends Component {
  static contextType = ThemeContext

  entryClass = "societies"
  defaultEntry = default_empty_society
  defaultOptions = default_options_societies
  requiredFields = required_fields_societies

  constructor(props) {
    super(props);
    this.state = {
      ...this.defaultEntry,
      ...this.defaultOptions
    }

    this.changeValue = this.changeValue.bind(this)
    this.getOptions = this.getOptions.bind(this)
    this.submit = this.submit.bind(this)
  }

  fancyEntryClass = toFancyEntryClass(this.entryClass)

  componentDidMount(){
    consoleLogDebug('Mounting ' + this.fancyEntryClass)
    this.getOptions()
  }

  async getOptions() {
    let societyTypesList = await getSimpleOptions( "societyType" )
    this.setState({societyTypeOptions: societyTypesList})

    let locationsList = await getReducedOptions( '/world_entries/locations', "relatedLocations" )
    this.setState({relatedLocationsOptions: locationsList})

    let individualsList = await getReducedOptions( 'individuals', "associatedIndividuals" )
    this.setState({associatedIndividualsOptions: individualsList})

    let societiesList = await getReducedOptions( 'societies', "relatedSocieties" )
    this.setState({relatedSocietiesOptions: societiesList})

    let objectsList = await getReducedOptions( 'objects', "notableTreasure" )
    this.setState({notableTreasureOptions: objectsList})
  }

  async submit() {
    let entry = {}
    for (const key of Object.keys(this.defaultEntry)) { 
      try {
        if (this.state[key] !== this.defaultEntry[key] || typeof this.state[key] === "boolean") {
          entry[key] = this.state[key] 
        }
      } catch {
          consoleLogDebug(`Problem with key ${key}`)
      }
    }
    entry.entryClass = this.entryClass
    try{
      if ( checkEntry(entry, this.requiredFields) ) {
        let success = await sendEntry(entry, this.entryClass)
        if ( success ) { this.props.changePage("board") }
      } 
    } catch(err){ consoleLogDebug(err)}
  }

  changeValue ( event, value ) {
    let id
    try { // Case needed for Select component of Materials-UI
      id = value.props.id.split("-")[0];
      consoleLogDebug(`Caso 1: ${id} - ${event.target.value}`, 1)
      this.setState({[id]: event.target.value})
    } catch {
      id = event.target.id.split("-")[0]
      if (value) { // Case needed for Autoselect component of Materials-UI
        consoleLogDebug(`Caso 2: ${id} - ${value}`, 1)
        this.setState({[id]: value})
      } else if (value !== false) { // Case needed for any Text Field component of Materials-UI ^^
        consoleLogDebug(`Caso 3: ${id} - ${event.target.value}`, 1)
        this.setState({[id]: event.target.value})
      } else { // Case needed for Bynary Field component
        consoleLogDebug(`Caso 4: ${id} - ${value}`, 1)
        this.setState({[id]: value})
      }
    }
  }

  render(){
    return (

      <div style={{ ...scrollStyle, ...divFlexStyle }} >

        <TitleField style={{ ...headerStyle, ...rowFlexStyle }}
          id="title"
          entryClass={this.entryClass} 
        /> 
      
        <NameField
            id="name"
            label={`Nombre de ${this.fancyEntryClass}*`}
            style={rowFlexStyle}
            value={this.state.name} 
            onChange={this.changeValue}
        />

        <ShortDescriptionField
            id="publicDescription"
            label="Breve descripción pública*"
            style={rowFlexStyle}
            value={this.state.publicDescription} 
            onChange={this.changeValue}
        />

        <SuggestSelectField
          id="societyType"
          label="Tipo*"
          style={rowFlexStyle}
          allowNew={true}
          options={this.state.societyTypeOptions}
          value={this.state.societyType}
          onChange={this.changeValue}
        />

        <HighMediumLowField
          id="danger"
          label="Peligro*"
          isMasculine={true}
          style={rowSym2FlexStyle[0]}
          value={this.state.danger}
          onChange={this.changeValue}
        />

        <HighMediumLowField
          id="influence"
          label="Influencia*"
          isMasculine={false}
          style={rowSym2FlexStyle[1]}
          value={this.state.influence}
          onChange={this.changeValue}
        />

        <HighMediumLowField
          id="members"
          label="Cantidad de miembros*"
          isMasculine={false}
          style={rowSym2FlexStyle[0]}
          value={this.state.members}
          onChange={this.changeValue}
        />

        <HighMediumLowField
          id="secretism"
          label="Secretismo*"
          isMasculine={true}
          style={rowSym2FlexStyle[1]}
          value={this.state.secretism}
          onChange={this.changeValue}
        />

        <LongDescriptionField
            id="longDescription"
            label="Descripción privada"
            style={rowFlexStyle}
            value={this.state.longDescription} 
            onChange={this.changeValue}
        />

        <TagsField
          id="relatedLocations"
          label="Locaciones relacionadas"
          style={rowFlexStyle}
          options={this.state.relatedLocationsOptions}
          getOptionLabel={getReducedOptionLabel}
          value={this.state.relatedLocations}
          onChange={this.changeValue}
        />

        <TagsField
          id="associatedIndividuals"
          label="Individuos relacionados"
          style={rowFlexStyle}
          options={this.state.associatedIndividualsOptions}
          getOptionLabel={getReducedOptionLabel}
          value={this.state.associatedIndividuals}
          onChange={this.changeValue}
        />
        
        <TagsField
          id="relatedSocieties"
          label="Sociedades relacionadas"
          style={rowFlexStyle}
          options={this.state.relatedSocietiesOptions}
          getOptionLabel={getReducedOptionLabel}
          value={this.state.relatedSocieties}
          onChange={this.changeValue}
        />

        <TagsField
          id="notableTreasure"
          label="Tesoro notable"
          style={rowFlexStyle}
          options={this.state.notableTreasureOptions}
          getOptionLabel={getReducedOptionLabel}
          value={this.state.notableTreasure}
          onChange={this.changeValue}
        />

        <Button 
          variant="outlined" 
          size="large" 
          style={{ ...rowFlexStyle, ...colorGreen }}  
          onClick={this.submit}
        >
          Crear {this.fancyEntryClass}
        </Button>
      
      </div>
    )
  }
}

export default CreateSociety;
export {default_empty_society}
