import React, { Component } from 'react'
import { consoleLogDebugCreator } from "../../../../logger"
import { ThemeContext } from "../../../../context"
import { scrollStyle, headerStyle, divFlexStyle, rowFlexStyle, rowASym2FlexStyle, colorGreen } from "../../style"
import { getReducedOptionLabel, getReducedOptions, checkEntry, sendEntry } from './create_methods'
import { TitleField, NameField, ShortDescriptionField, 
         LongDescriptionField, HighMediumLowField, TagsField } from "../../components/create_components"
import { toFancyEntryClass, default_locations, default_individuals, default_societies, default_objects } from "../../components/world_entry_consts"
import Button from '@material-ui/core/Button';

const verbosityLevel = 0;
const consoleLogDebug = consoleLogDebugCreator(verbosityLevel);

const default_empty_event = {
  name:'',
  publicDescription:'',
  impact:'',
  happenedIn:[],
  keyInformation:'',
  longDescription:'',
  involvedIndividuals: [],
  involvedSocieties: [],
  involvedObjects: [],
  changes:'',
  diffusion:[],
  testimonies:[],
}

const default_options_events = {
  happenedInOptions: default_locations,
  involvedIndividualOptions: default_individuals,
  involvedSocietyOptions: default_societies,
  involvedObjectOptions: default_objects,
}

const required_fields_events = ["name", "publicDescription", "impact", "keyInformation"]

class CreateEvent extends Component {
  static contextType = ThemeContext

  entryClass = "events"
  defaultEntry = default_empty_event
  defaultOptions = default_options_events
  requiredFields = required_fields_events

  constructor(props) {
    super(props);
    this.state = {
      ...this.defaultEntry,
      ...this.defaultOptions
    }

    this.changeValue = this.changeValue.bind(this)
    this.getOptions = this.getOptions.bind(this)
    this.submit = this.submit.bind(this)
  }

  fancyEntryClass = toFancyEntryClass(this.entryClass)

  componentDidMount(){
    consoleLogDebug('Mounting ' + this.fancyEntryClass)
    this.getOptions()
  }

  async getOptions() {
    let locationsList = await getReducedOptions( 'locations', "happenedIn" )
    this.setState({happenedInOptions: locationsList})

    let individualsList = await getReducedOptions( 'individuals', "involvedIndividual" )
    this.setState({involvedIndividualOptions: individualsList})

    let societiesList = await getReducedOptions( 'societies', "involvedSociety" )
    this.setState({involvedSocietyOptions: societiesList})

    let objectsList = await getReducedOptions( 'objects', "involvedObject" )
    this.setState({involvedObjectOptions: objectsList})

  }

  async submit() {
    let entry = {}
    for (const key of Object.keys(this.defaultEntry)) { 
      try {
        if (this.state[key] !== this.defaultEntry[key] || typeof this.state[key] === "boolean") {
          entry[key] = this.state[key] 
        }
      } catch {
          consoleLogDebug(`Problem with key ${key}`)
      }
    }
    entry.entryClass = this.entryClass
    try{
      if ( checkEntry(entry, this.requiredFields) ) {
        let success = await sendEntry(entry, this.entryClass)
        if ( success ) { this.props.changePage("board") }
      } 
    } catch(err){ consoleLogDebug(err)}
  }

  changeValue ( event, value ) {
    let id
    try { // Case needed for Select component of Materials-UI
      id = value.props.id.split("-")[0];
      consoleLogDebug(`Caso 1: ${id} - ${event.target.value}`, 1)
      this.setState({[id]: event.target.value})
    } catch {
      id = event.target.id.split("-")[0]
      if (value) { // Case needed for Autoselect component of Materials-UI
        consoleLogDebug(`Caso 2: ${id} - ${value}`, 1)
        this.setState({[id]: value})
      } else if (value !== false) { // Case needed for any Text Field component of Materials-UI ^^
        consoleLogDebug(`Caso 3: ${id} - ${event.target.value}`, 1)
        this.setState({[id]: event.target.value})
      } else { // Case needed for Bynary Field component
        consoleLogDebug(`Caso 4: ${id} - ${value}`, 1)
        this.setState({[id]: value})
      }
    }
  }

  render(){
    return (

      <div style={{ ...scrollStyle, ...divFlexStyle }} >

        <TitleField style={{ ...headerStyle, ...rowFlexStyle }}
          id="title"
          entryClass={this.entryClass} 
        /> 

        <NameField
            id="name"
            label={`Nombre de ${this.fancyEntryClass}*`}
            style={rowFlexStyle}
            value={this.state.name} 
            onChange={this.changeValue}
        />

        <ShortDescriptionField
            id="publicDescription"
            label="Breve descripción pública*"
            style={rowFlexStyle}
            value={this.state.publicDescription} 
            onChange={this.changeValue}
        />
      
        <HighMediumLowField
          id="impact"
          label="Impacto*"
          isMasculine={true}
          style={rowASym2FlexStyle[0]}
          value={this.state.impact}
          onChange={this.changeValue}
        />

        <TagsField
          id="happenedIn"
          label="Sitio/s en el/los que ocurrió"
          style={rowASym2FlexStyle[1]}
          options={this.state.happenedInOptions}
          getOptionLabel={getReducedOptionLabel}
          value={this.state.happenedIn}
          onChange={this.changeValue}
        />

        <LongDescriptionField
            id="keyInformation"
            label="Información clave*"
            style={rowFlexStyle}
            value={this.state.keyInformation} 
            onChange={this.changeValue}
        />

        <LongDescriptionField
            id="longDescription"
            label="Descripción privada"
            style={rowFlexStyle}
            value={this.state.longDescription} 
            onChange={this.changeValue}
        />

        <TagsField
          id="involvedIndividuals"
          label="Individuos involucrado/as"
          style={rowFlexStyle}
          options={this.state.involvedIndividualOptions}
          getOptionLabel={getReducedOptionLabel}
          value={this.state.involvedIndividuals}
          onChange={this.changeValue}
        />

        <TagsField
          id="involvedSocieties"
          label="Sociedades involucradas"
          style={rowFlexStyle}
          options={this.state.involvedSocietyOptions}
          getOptionLabel={getReducedOptionLabel}
          value={this.state.involvedSocieties}
          onChange={this.changeValue}
        />

        <TagsField
          id="involvedObjects"
          label="Objetos involucrados"
          style={rowFlexStyle}
          options={this.state.involvedObjectOptions}
          getOptionLabel={getReducedOptionLabel}
          value={this.state.involvedObjects}
          onChange={this.changeValue}
        />

        <LongDescriptionField
            id="changes"
            label="Cambios ocasionados"
            style={rowFlexStyle}
            value={this.state.changes} 
            onChange={this.changeValue}
        />

        <Button 
          variant="outlined" 
          size="large" 
          style={{ ...rowFlexStyle, ...colorGreen }}  
          onClick={this.submit}
        >
          Crear {this.fancyEntryClass}
        </Button>
      
      </div>
    )
  }
}

export default CreateEvent;
export { default_empty_event }