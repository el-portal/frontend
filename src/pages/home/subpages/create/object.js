import React, { Component } from 'react'
import { consoleLogDebugCreator } from "../../../../logger"
import { ThemeContext } from "../../../../context"
import { scrollStyle, headerStyle, divFlexStyle, rowFlexStyle, rowSym2FlexStyle, rowASym2FlexStyle, colorGreen } from "../../style"
import { getReducedOptionLabel, getReducedOptions, getSimpleOptions, checkEntry, sendEntry } from './create_methods'
import { TitleField, NameField, ShortDescriptionField, LongDescriptionField,
         BinaryField, SelectField, HighMediumLowField, SuggestSelectField, TagsField } from "../../components/create_components"
import { toFancyEntryClass, default_locations, default_individuals, default_societies } from "../../components/world_entry_consts"
import Button from '@material-ui/core/Button';

const verbosityLevel = 0;
const consoleLogDebug = consoleLogDebugCreator(verbosityLevel);

const default_object_types = ["Espada", "Anillo", "Arco", "Poción"];

const default_empty_object = {
  name:'',
  publicDescription:'',
  influence:'',
  danger:'',
  objectType:'',
  magic:false,
  magicRarity:'',
  longDescription:'',
  possibleLocations:[],
  possibleIndividualOwners:[],
  possibleSocietyOwners:[],
}

const default_options_objects = {
  objectTypeOptions: default_object_types,
  possibleLocationsOptions: default_locations,
  possibleIndividualOwnersOptions: default_individuals,
  possibleSocietyOwnersOptions: default_societies,
}

const required_fields_objects = ["name", "publicDescription", "influence", "danger", "objectType"]

class CreateObject extends Component {
  static contextType = ThemeContext

  entryClass = "objects"
  defaultEntry = default_empty_object
  defaultOptions = default_options_objects
  requiredFields = required_fields_objects

  constructor(props) {
    super(props);
    this.state = {
      ...this.defaultEntry,
      ...this.defaultOptions
    }

    this.changeValue = this.changeValue.bind(this)
    this.getOptions = this.getOptions.bind(this)
    this.submit = this.submit.bind(this)
  }

  fancyEntryClass = toFancyEntryClass(this.entryClass)

  componentDidMount(){
    consoleLogDebug('Mounting ' + this.fancyEntryClass)
    this.getOptions()
  }

  async getOptions() {
    let objectTypesList = await getSimpleOptions( "objectType" )
    this.setState({objectTypeOptions: objectTypesList})

    let locationsList = await getReducedOptions( 'locations', "possibleLocations" )
    this.setState({possibleLocationsOptions: locationsList})

    let individualsList = await getReducedOptions( 'individuals', "possibleIndividualOwners" )
    this.setState({possibleIndividualOwnersOptions: individualsList})

    let societiesList = await getReducedOptions( 'societies', "possibleSocietyOwners" )
    this.setState({possibleSocietyOwnersOptions: societiesList})
  }

  async submit() {
    let entry = {}
    for (const key of Object.keys(this.defaultEntry)) { 
      try {
        if (this.state[key] !== this.defaultEntry[key] || typeof this.state[key] === "boolean") {
          entry[key] = this.state[key] 
        }
      } catch {
          consoleLogDebug(`Problem with key ${key}`)
      }
    }
    entry.entryClass = this.entryClass
    try{
      if ( checkEntry(entry, this.requiredFields) ) {
        let success = await sendEntry(entry, this.entryClass)
        if ( success ) { this.props.changePage("board") }
      } 
    } catch(err){ consoleLogDebug(err)}
  }

  changeValue ( event, value ) {
    let id
    try { // Case needed for Select component of Materials-UI
      id = value.props.id.split("-")[0];
      consoleLogDebug(`Caso 1: ${id} - ${event.target.value}`, 1)
      this.setState({[id]: event.target.value})
    } catch {
      id = event.target.id.split("-")[0]
      if (value) { // Case needed for Autoselect component of Materials-UI
        consoleLogDebug(`Caso 2: ${id} - ${value}`, 1)
        this.setState({[id]: value})
      } else if (value !== false) { // Case needed for any Text Field component of Materials-UI ^^
        consoleLogDebug(`Caso 3: ${id} - ${event.target.value}`, 1)
        this.setState({[id]: event.target.value})
      } else { // Case needed for Bynary Field component
        consoleLogDebug(`Caso 4: ${id} - ${value}`, 1)
        this.setState({[id]: value})
      }
    }
  }

  render(){
    return (

      <div style={{ ...scrollStyle, ...divFlexStyle }} >

        <TitleField style={{ ...headerStyle, ...rowFlexStyle }}
          id="title"
          entryClass={this.entryClass} 
        /> 
      
        <NameField
            id="name"
            label={`Nombre de ${this.fancyEntryClass}*`}
            style={rowFlexStyle}
            value={this.state.name} 
            onChange={this.changeValue}
        />

        <ShortDescriptionField
            id="publicDescription"
            label="Breve descripción pública*"
            style={rowFlexStyle}
            value={this.state.publicDescription} 
            onChange={this.changeValue}
        />

        <HighMediumLowField
          id="influence"
          label="Influencia*"
          isMasculine={false}
          style={rowSym2FlexStyle[0]}
          value={this.state.influence}
          onChange={this.changeValue}
        />

        <HighMediumLowField
          id="danger"
          label="Peligro*"
          isMasculine={true}
          style={rowSym2FlexStyle[1]}
          value={this.state.danger}
          onChange={this.changeValue}
        />

        <SuggestSelectField
          id="objectType"
          label="Tipo de objeto*"
          style={rowFlexStyle}
          allowNew={true}
          options={this.state.objectTypeOptions}
          value={this.state.objectType}
          onChange={this.changeValue}
        />

        <BinaryField
          id="magic"
          label="Mágico"
          style={rowASym2FlexStyle[0]}
          value={this.state.magic}
          onChange={this.changeValue}
        />  

        <SelectField
          id="magicRarity"
          label="Rareza mágica"
          selectValues={["Común", "No común", "Raro", "Muy raro", "Legendario"]}
          style={rowASym2FlexStyle[1]}
          value={this.state.magicRarity}
          onChange={this.changeValue}
          disabled={!(this.state.magic)}
        />        

        <LongDescriptionField
            id="longDescription"
            label="Descripción privada*"
            style={rowFlexStyle}
            value={this.state.longDescription} 
            onChange={this.changeValue}
        />

        <TagsField
          id="possibleLocations"
          label="Posibles locaciones"
          style={rowFlexStyle}
          options={this.state.possibleLocationsOptions}
          getOptionLabel={getReducedOptionLabel}
          value={this.state.possibleLocations}
          onChange={this.changeValue}
        />

        <TagsField
          id="possibleIndividualOwners"
          label="Posibles individuos poseedores"
          style={rowFlexStyle}
          options={this.state.possibleIndividualOwnersOptions}
          getOptionLabel={getReducedOptionLabel}
          value={this.state.possibleIndividualOwners}
          onChange={this.changeValue}
        />

        <TagsField
          id="possibleSocietyOwners"
          label="Posibles sociedades poseedoras"
          style={rowFlexStyle}
          options={this.state.possibleSocietyOwnersOptions}
          getOptionLabel={getReducedOptionLabel}
          value={this.state.possibleSocietyOwners}
          onChange={this.changeValue}
        />

        <Button 
          variant="outlined" 
          size="large" 
          style={{ ...rowFlexStyle, ...colorGreen }}  
          onClick={this.submit}
        >
          Crear {this.fancyEntryClass}
        </Button>
      
      </div>
    )
  }
}

export default CreateObject;
export { default_empty_object }