import React, { Component } from 'react'
import { consoleLogDebugCreator } from "../../../../logger"
import { ThemeContext } from "../../../../context"
import { scrollStyle, headerStyle, divFlexStyle, rowFlexStyle, rowSym2FlexStyle, colorGreen } from "../../style"
import { getReducedOptionLabel, getReducedOptions, getSimpleOptions, checkEntry, sendEntry } from './create_methods'
import { TitleField, NameField, ShortDescriptionField, LongDescriptionField,
         HighMediumLowField, TagsField, SuggestSelectField } from "../../components/create_components"
import { toFancyEntryClass, default_locations, default_individuals, default_objects, default_societies } from "../../components/world_entry_consts"
import Button from '@material-ui/core/Button';

const verbosityLevel = 0;
const consoleLogDebug = consoleLogDebugCreator(verbosityLevel);

const default_location_types = ["Reino", "Ciudad", "Pueblo", "Punto Clave"];

const default_empty_location = {
  name:'',
  publicDescription:'',
  locationType:'',
  x: Math.floor(Math.random() * 85 + 8.5),
  y: Math.floor(Math.random() * 85 + 8.5),
  fame:'',
  danger:'',
  longDescription:'',
  relatedIndividuals:[],
  relatedSocieties:[],
  notableTreasure:[],
  includedInLocations:[],
  includesLocations:[],
}

const default_options_locations = {
  locationTypeOptions: default_location_types,
  relatedIndividualsOptions: default_individuals,
  relatedSocietiesOptions: default_societies,
  notableTreasureOptions: default_objects,
  includedInLocationsOptions: default_locations,
  includesLocationsOptions: default_locations,
}

const required_fields_locations = ["name", "publicDescription", "locationType", "fame", "danger"]

class CreateLocation extends Component {
  static contextType = ThemeContext

  entryClass = "locations"
  defaultEntry = default_empty_location
  defaultOptions = default_options_locations
  requiredFields = required_fields_locations

  constructor(props) {
    super(props);
    this.state = {
      ...this.defaultEntry,
      ...this.defaultOptions
    }

    this.changeValue = this.changeValue.bind(this)
    this.getOptions = this.getOptions.bind(this)
    this.submit = this.submit.bind(this)
  }

  fancyEntryClass = toFancyEntryClass(this.entryClass)

  componentDidMount(){
    consoleLogDebug('Mounting ' + this.fancyEntryClass)
    this.getOptions()
  }

  async getOptions() {
    let locationTypesList = await getSimpleOptions( "locationType" )
    this.setState({locationTypeOptions: locationTypesList})

    let individualsList = await getReducedOptions( 'individuals', "relatedIndividuals" )
    this.setState({relatedIndividualsOptions: individualsList})

    let societiesList = await getReducedOptions( 'societies', "relatedSocieties" )
    this.setState({relatedSocietiesOptions: societiesList})

    let objectsList = await getReducedOptions( 'objects', "notableTresure" )
    this.setState({notableTreasureOptions: objectsList})

    let locationsList = await getReducedOptions( 'locations', "includedInLocations" )
    this.setState({includedInLocationsOptions: locationsList})
    this.setState({includesLocationsOptions: locationsList})
  }

  async submit() {
    let entry = {}
    for (const key of Object.keys(this.defaultEntry)) { 
      try {
        if (this.state[key] !== this.defaultEntry[key] || typeof this.state[key] === "boolean") {
          entry[key] = this.state[key] 
        }
      } catch {
          consoleLogDebug(`Problem with key ${key}`)
      }
    }
    entry.entryClass = this.entryClass
    try{
      if ( checkEntry(entry, this.requiredFields) ) {
        let success = await sendEntry(entry, this.entryClass)
        if ( success ) { this.props.changePage("board") }
      } 
    } catch(err){ consoleLogDebug(err)}
  }

  changeValue ( event, value ) {
    let id
    try { // Case needed for Select component of Materials-UI
      id = value.props.id.split("-")[0];
      consoleLogDebug(`Caso 1: ${id} - ${event.target.value}`, 1)
      this.setState({[id]: event.target.value})
    } catch {
      id = event.target.id.split("-")[0]
      if (value) { // Case needed for Autoselect component of Materials-UI
        consoleLogDebug(`Caso 2: ${id} - ${value}`, 1)
        this.setState({[id]: value})
      } else if (value !== false) { // Case needed for any Text Field component of Materials-UI ^^
        consoleLogDebug(`Caso 3: ${id} - ${event.target.value}`, 1)
        this.setState({[id]: event.target.value})
      } else { // Case needed for Bynary Field component
        consoleLogDebug(`Caso 4: ${id} - ${value}`, 1)
        this.setState({[id]: value})
      }
    }
  }

  render(){
    return (

      <div style={{ ...scrollStyle, ...divFlexStyle }} >

        <TitleField style={{ ...headerStyle, ...rowFlexStyle }}
          id="title"
          entryClass={this.entryClass} 
        /> 
      
        <NameField
            id="name"
            label={`Nombre de ${this.fancyEntryClass}*`}
            style={rowFlexStyle}
            value={this.state.name} 
            onChange={this.changeValue}
        />

        <ShortDescriptionField
            id="publicDescription"
            label="Breve descripción pública*"
            style={rowFlexStyle}
            value={this.state.publicDescription} 
            onChange={this.changeValue}
        />

        <SuggestSelectField
          id="locationType"
          label="Tipo*"
          style={rowFlexStyle}
          allowNew={true}
          options={this.state.locationTypeOptions}
          value={this.state.locationType}
          onChange={this.changeValue}
        />

        <HighMediumLowField
          id="fame"
          label="Fama*"
          isMasculine={false}
          style={rowSym2FlexStyle[0]}
          value={this.state.fame}
          onChange={this.changeValue}
        />

        <HighMediumLowField
          id="danger"
          label="Peligro*"
          isMasculine={true}
          style={rowSym2FlexStyle[1]}
          value={this.state.danger}
          onChange={this.changeValue}
        />

        <LongDescriptionField
            id="longDescription"
            label="Descripción privada"
            style={rowFlexStyle}
            value={this.state.longDescription} 
            onChange={this.changeValue}
        />

        <TagsField
          id="relatedIndividuals"
          label="Individuos relacionados"
          style={rowFlexStyle}
          options={this.state.relatedIndividualsOptions}
          getOptionLabel={getReducedOptionLabel}
          value={this.state.relatedIndividuals}
          onChange={this.changeValue}
        />


        <TagsField
          id="relatedSocieties"
          label="Sociedades relacionadas"
          style={rowFlexStyle}
          options={this.state.relatedSocietiesOptions}
          getOptionLabel={getReducedOptionLabel}
          value={this.state.relatedSocieties}
          onChange={this.changeValue}
        />

        <TagsField
          id="notableTreasure"
          label="Tesoros notables"
          style={rowFlexStyle}
          options={this.state.notableTreasureOptions}
          getOptionLabel={getReducedOptionLabel}
          value={this.state.notableTreasure}
          onChange={this.changeValue}
        />

        <TagsField
          id="includedInLocations"
          label="Locaciones que incluyen a esta locación"
          style={rowFlexStyle}
          options={this.state.includedInLocationsOptions}
          getOptionLabel={getReducedOptionLabel}
          value={this.state.includedInLocations}
          onChange={this.changeValue}
        />

        <TagsField
          id="includesLocations"
          label="Locaciones incluidas dentro de esta locación"
          style={rowFlexStyle}
          options={this.state.includesLocationsOptions}
          getOptionLabel={getReducedOptionLabel}
          value={this.state.includesLocations}
          onChange={this.changeValue}
        />

        <Button 
          variant="outlined" 
          size="large" 
          style={{ ...rowFlexStyle, ...colorGreen }}  
          onClick={this.submit}
        >
          Crear {this.fancyEntryClass}
        </Button>
      
      </div>
    )
  }
}

export default CreateLocation;
export { default_empty_location }