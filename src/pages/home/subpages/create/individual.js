import React, { Component } from 'react'
import { consoleLogDebugCreator } from "../../../../logger"
import { ThemeContext } from "../../../../context"
import { scrollStyle, headerStyle, divFlexStyle, rowFlexStyle, rowSym2FlexStyle, colorGreen } from "../../style"
import { getReducedOptionLabel, getReducedOptions, getSimpleOptions, checkEntry, sendEntry } from './create_methods'
import { TitleField, NameField, ShortDescriptionField, LongDescriptionField,
         HighMediumLowField, TagsField, SuggestSelectField } from "../../components/create_components"
import { toFancyEntryClass, default_locations, default_individuals, default_societies, default_objects } from "../../components/world_entry_consts"
import Button from '@material-ui/core/Button';

const verbosityLevel = 1;
const consoleLogDebug = consoleLogDebugCreator(verbosityLevel);

const default_races = ["Humano/a", "Semielfo/a", "Elfo/a", "Enano/a", "Gnomo/a", "Mediano/a", "Dracónido/a", "Tiefling", "Semiorco/a"]
const default_occupations = ["Aventurero/a", "Guardia", "Tabernero/a", "Granjero/a"];

const default_empty_individual = {
  name:'',
  publicDescription:'',
  race:'',
  occupation:'',
  influence:'',
  danger:'',
  longDescription:'',
  usualLocations:[],
  relatedIndividuals:[],
  associatedSocieties: [],
  notableInventory: [],
}

const default_options_individuals = {
  raceOptions: default_races,
  occupationOptions: default_occupations,
  usualLocationsOptions: default_locations,
  relatedIndividualsOptions: default_individuals,
  associatedSocietiesOptions: default_societies,
  notableInventoryOptions: default_objects,
}

const required_fields_individuals = ["name", "publicDescription", "race", "occupation", "influence", "danger"]

class CreateIndividual extends Component {
  static contextType = ThemeContext

  entryClass = "individuals"
  defaultEntry = default_empty_individual
  defaultOptions = default_options_individuals
  requiredFields = required_fields_individuals

  constructor(props) {
    super(props);
    this.state = {
      ...this.defaultEntry,
      ...this.defaultOptions
    }

    this.changeValue = this.changeValue.bind(this)
    this.getOptions = this.getOptions.bind(this)
    this.submit = this.submit.bind(this)
  }

  fancyEntryClass = toFancyEntryClass(this.entryClass)

  componentDidMount(){
    consoleLogDebug('Mounting ' + this.fancyEntryClass)
    this.getOptions()
  }

  async getOptions() {
    let racesList = await getSimpleOptions( "race" )
    this.setState({raceOptions: racesList})

    let occupationsList = await getSimpleOptions( "occupation" )
    this.setState({occupationOptions: occupationsList})

    let locationsList = await getReducedOptions( 'locations', "usualLocations" )
    this.setState({usualLocationsOptions: locationsList})

    let individualsList = await getReducedOptions( 'individuals', "relatedIndividuals" )
    this.setState({relatedIndividualsOptions: individualsList})

    let societiesList = await getReducedOptions( 'societies', "associatedSocieties" )
    this.setState({associatedSocietiesOptions: societiesList})

    let objectsList = await getReducedOptions( 'objects', "notableInventory" )
    this.setState({notableInventoryOptions: objectsList})
  }

  async submit() {
    let entry = {}
    for (const key of Object.keys(this.defaultEntry)) { 
      try {
        if (this.state[key] !== this.defaultEntry[key] || typeof this.state[key] === "boolean") {
          entry[key] = this.state[key] 
        }
      } catch {
          consoleLogDebug(`Problem with key ${key}`)
      }
    }
    entry.entryClass = this.entryClass
    try{
      if ( checkEntry(entry, this.requiredFields) ) {
        let success = await sendEntry(entry, this.entryClass)
        if ( success ) { this.props.changePage("board") }
      } 
    } catch(err){ consoleLogDebug(err)}
  }

  changeValue ( event, value ) {
    let id
    try { // Case needed for Select component of Materials-UI
      id = value.props.id.split("-")[0];
      consoleLogDebug(`Caso 1: ${id} - ${event.target.value}`, 2)
      this.setState({[id]: event.target.value})
    } catch {
      id = event.target.id.split("-")[0]
      if (value) { // Case needed for Autoselect component of Materials-UI
        consoleLogDebug(`Caso 2: ${id} - ${value}`, 2)
        this.setState({[id]: value})
      } else if (value !== false) { // Case needed for any Text Field component of Materials-UI ^^
        consoleLogDebug(`Caso 3: ${id} - ${event.target.value}`, 2)
        this.setState({[id]: event.target.value})
      } else { // Case needed for Bynary Field component
        consoleLogDebug(`Caso 4: ${id} - ${value}`, 2)
        this.setState({[id]: value})
      }
    }
  }

  render(){
    return (

      <div style={{ ...scrollStyle, ...divFlexStyle }} >

        <TitleField style={{ ...headerStyle, ...rowFlexStyle }}
          id="title"
          entryClass={this.entryClass} 
        /> 
      
        <NameField
            id="name"
            label={`Nombre de ${this.fancyEntryClass}*`}
            style={rowFlexStyle}
            value={this.state.name} 
            onChange={this.changeValue}
        />

        <ShortDescriptionField
            id="publicDescription"
            label="Breve descripción pública*"
            style={rowFlexStyle}
            value={this.state.publicDescription} 
            onChange={this.changeValue}
        />

        <SuggestSelectField
          id="race"
          label="Raza*"
          style={rowSym2FlexStyle[0]}
          allowNew={true}
          options={this.state.raceOptions}
          value={this.state.race}
          onChange={this.changeValue}
        />

        <SuggestSelectField
          id="occupation"
          label="Ocupación*"
          style={rowSym2FlexStyle[1]}
          allowNew={true}
          options={this.state.occupationOptions}
          value={this.state.occupation}
          onChange={this.changeValue}
        />

        <HighMediumLowField
          id="influence"
          label="Influencia*"
          isMasculine={false}
          style={rowSym2FlexStyle[0]}
          value={this.state.influence}
          onChange={this.changeValue}
        />

        <HighMediumLowField
          id="danger"
          label="Peligro*"
          isMasculine={true}
          style={rowSym2FlexStyle[1]}
          value={this.state.danger}
          onChange={this.changeValue}
        />

        <LongDescriptionField
            id="longDescription"
            label="Descripción privada"
            style={rowFlexStyle}
            value={this.state.longDescription} 
            onChange={this.changeValue}
        />

        <TagsField
          id="usualLocations"
          label="Locaciones usuales"
          style={rowFlexStyle}
          options={this.state.usualLocationsOptions}
          getOptionLabel={getReducedOptionLabel}
          value={this.state.usualLocations}
          onChange={this.changeValue}
        />

        <TagsField
          id="relatedIndividuals"
          label="Individuos relacionados"
          style={rowFlexStyle}
          options={this.state.relatedIndividualsOptions}
          getOptionLabel={getReducedOptionLabel}
          value={this.state.relatedIndividuals}
          onChange={this.changeValue}
        />
        
        <TagsField
          id="associatedSocieties"
          label="Sociedades a las que pertenece"
          style={rowFlexStyle}
          options={this.state.associatedSocietiesOptions}
          getOptionLabel={getReducedOptionLabel}
          value={this.state.associatedSocieties}
          onChange={this.changeValue}
        />

        <TagsField
          id="notableInventory"
          label="Inventario notable"
          style={rowFlexStyle}
          options={this.state.notableInventoryOptions}
          getOptionLabel={getReducedOptionLabel}
          value={this.state.notableInventory}
          onChange={this.changeValue}
        />

        <Button 
          variant="outlined" 
          size="large" 
          style={{ ...rowFlexStyle, ...colorGreen }}  
          onClick={this.submit}
        >
          Crear {this.fancyEntryClass}
        </Button>
      
      </div>
    )
  }
}

export default CreateIndividual;
export { default_empty_individual }
