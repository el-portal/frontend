import axios from 'axios'
import { toFancyEntryClass } from '../../components/world_entry_consts'
import { config } from "../../../../config" 
import { consoleLogDebugCreator } from "../../../../logger"

const verbosityLevel = 1
const consoleLogDebug = consoleLogDebugCreator(verbosityLevel)

const getReducedOptionLabel = (option) => ( option.name )

const getReducedOptions = ( entryClass, field ) => { return new Promise(async (resolve, reject) => {
  const url = '/world_entries/entries/get/class'
  consoleLogDebug(config.URL_BASE, 2)
  let reducedOptions
  await axios.post( config.URL_BASE + url , {'entryClass': entryClass} ).then( (response) => {
      reducedOptions = response.data
      consoleLogDebug(`Éxito al recuperar las opciones de '${field}'`, 1)
      try {
        consoleLogDebug(`La primera es '${reducedOptions[0].name}' con id ${reducedOptions[0]._id}`, 1) 
      } catch {
        consoleLogDebug(`Aunque parece una lista vacía :/`, 1) 
      }
      // this.setState({[field]: entries})
    }).catch ((err)=>{
      consoleLogDebug(`Error al recuperar las opciones de '${field}'`) 
      reject(err)
    })
    resolve( reducedOptions )
})}

// const getSimpleOptionLabel = (option) => { option }

const getSimpleOptions = ( field ) => { return new Promise(async (resolve, reject) => {
  const url = "/world_entries/options/get/field" 
  consoleLogDebug(config.URL_BASE, 2)
  let simpleOptions
  await axios.post( config.URL_BASE + url , {"field":field} ).then( (response) => {
      simpleOptions = response.data
      consoleLogDebug(`Éxito al recuperar las opciones de '${field}'`, 1)
      try {
        consoleLogDebug(`La primera es '${simpleOptions[0]}'`, 1) 
      } catch {
        consoleLogDebug(`Aunque parece una lista vacía :/`, 1) 
      }
      // this.setState({[keyOptions]: entries})
    }).catch ((err)=>{
      consoleLogDebug(`Error al recuperar las opciones de '${field}'`) 
      reject(err)
    })
  resolve( simpleOptions )
})}

function checkEntry( entryState, requiredFields=[] ) {

  const singleCheck = (field) => {
    try {
      return( entryState[field] !== '' && entryState[field][0] !== undefined )
    } catch {
      return (false)
    }
  }

  let verified = requiredFields.every( field  => {
    if (typeof field == "string") {
      return ( singleCheck(field) )
    } else {
      try {
        const subfields_verified = []
        for (const subfield of field) { subfields_verified.push( singleCheck(subfield) ) }
        return ( subfields_verified.some( result => result ) )
      } catch {
        return ( false )
      }
    }
  })
  
  if (!verified) { consoleLogDebug("¡Hay campos requeridos vacíos!") }
  
  return verified
}

async function sendEntry( entry ) { return new Promise(async (resolve, reject) => {

  consoleLogDebug(config.URL_BASE, 2)
  consoleLogDebug(entry, 2)

  let success
  await axios.post( config.URL_BASE + `world_entries/world/create` ,  {"entry":entry} ).then( (response) => {
    if (response.status < 300 ) {
        consoleLogDebug(`Creación exitosa de ${toFancyEntryClass(entry.entryClass)}`, 1)
        success = true;
    } else {
        consoleLogDebug(`Error en creación de ${toFancyEntryClass(entry.entryClass)}`);
        success = false;
    }
  }).catch( (err) =>{
    success = false;  
    consoleLogDebug('Error al enviar ' + toFancyEntryClass(entry.entryClass));
    reject(err);
  })

  resolve( success )
})}

export { getReducedOptionLabel, getReducedOptions, getSimpleOptions, checkEntry, sendEntry, consoleLogDebugCreator };
