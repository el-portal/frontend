import React, { Component } from 'react'
import axios from 'axios'
import { config } from "../../../../config" 
import { consoleLogDebugCreator } from "../../../../logger"
import { ThemeContext } from "../../../../context"
import { scrollStyle, rowFlexStyle } from "../../style"
import { InnerLeftLayout } from '../../layouts'
import { Diffusion } from "./diffusion"
import { Testimonies } from "./testimonies"
import { BoardIcon, NewsIcon, SearchIcon } from "../../components/icon_components"
import { EntryChipField } from "../../components/show_components"
import { NewsListItem, ShowHideWrapper } from '../../components/news_components'
import { default_empty_event } from "../create/event"
import { toFancyNewsClass } from "../../components/world_entry_consts"

const verbosityLevel = 2;
const consoleLogDebug = consoleLogDebugCreator(verbosityLevel);

const default_event = {
  _id: "60c54250e331e7f6c1b85bf2",
  entryClass:'events',
  name:'Frodo obtiene el anillo',
  publicDescription:'Habiéndose marchado Bilbo, Frodo se encuentra en Bolsón Cerrado con Gandalf y con una carta que contiene un anillo muy singular.',
  impact:'Alto',
  keyInformation:'Frodo obtiene el Anillo Único',
  longDescription:'En una extraña despedida, Bilbo se marcha de repente desapareciendo de su cumpleaños. Frodo vuelve a Bolsón Cerrado y allí se encuentra con un Gandalf de aspecto preocupado. Le dice que Bilbo le ha dejado una carta y en ella encuentra un anillo muy singular. Instado por el mago, lo arroja al fuego y se revela una tétrica inscripción. Afectado, Gandalf decide ir a buscar más información, descubre a Sam espiando la conversación y les encarga a ambos irse velozmente de La Comarca.',
  changes:'Frodo ahora posee el Anillo Único',
  diffusion: [
    {
      diffusionType: "Rumor",
      verisimilitude: "Media",
      content: "Se rumorea que últimamente se ha visto muy nervioso a Sam Gamyi cuando entra y sale de Bolsón Cerrado. ¿Será por lo mismo que tiene a Frodo Bolsón más pálido que de costumbre?",
      created_ts: Date.now() + 1000 + Math.floor(Math.random() * 100)
    }
  ],
  testimonies: [
    { 
      content: '¡Es cierto! El otro día quise jugarle una broma tonta y sobresaltarlo mientras se encargaba del jardín, pero al instante levantó la pala de forma agresiva, con los ojos desorbitados, y gritó: "¡Con el señor Frodo no!"',
      witnessIndividual: [{name: 'Pippin Peregrin Tuk', _id:null, entryClass:"individuals"}],
      isIndividual: true,
      verisimilitude: "Alta",
      created_ts:  Date.now() + 1100 + Math.floor(Math.random() * 10)
    }, // Maybe 2 testimonies each?
    { 
      content: '¡No me digas! ¿Y qué creés que podrá haberles pasado? Porque yo ya cazé dos veces a Frodo mirando con gesto ausente hacia los caminos que salen de La Comarca y con el libro apretado dentro de una mano. Me parece que hasta lo vi tragar saliva.',
      witnessIndividual: [{name: 'Merry Meriadoc Brandigamo', _id:null, entryClass:"individuals"}],
      isIndividual: true,
      verisimilitude: "Alta",
      created_ts:  Date.now() + 1110 + Math.floor(Math.random() * 10)
    },
    { 
      content: 'La verdad que ni idea. Pero supongo que vale la pena investigar, Merry. ¿Qué tal si lo agarramos a Sam entre los dos? Es mucho más fácil de intimidar que Frodo. Je, de hecho no hubo vez que no hayamos logrado sacarle la información que queríamos.',
      witnessIndividual: [{name: 'Pippin Peregrin Tuk', _id:null, entryClass:"individuals"}],
      isIndividual: true,
      verisimilitude: "Alta",
      created_ts:  Date.now() + 1120 + Math.floor(Math.random() * 10)
    },
  ],
  created_ts: Date.now() //- Math.floor(Math.random() * 1000),
}

class News extends Component {
  static contextType = ThemeContext

  defaultEmpty = default_empty_event

  constructor(props) {
    super(props);

    let filledEntry = {}
    for (const key of Object.keys(this.defaultEmpty)) {
      try {
        if (typeof props.entry[key] === 'undefined') {
          filledEntry[key] = this.defaultEmpty[key]
        } else {
          filledEntry[key] = props.entry[key]
        }
      } catch {
        filledEntry[key] = this.defaultEmpty[key]
      }
    }
    filledEntry._id = props.entry._id
    filledEntry.entryClass = "events"

    this.state = {
      entry: { ...filledEntry },
    }

    this.send = this.send.bind(this)
  }

  componentDidMount(){
    consoleLogDebug(`Montando noticias de evento: ${this.state.entry.name}`)
  }

  async send(newsEntry, newsClass) { return new Promise(async (resolve, reject) => {

    consoleLogDebug(config.URL_BASE, 2)
    consoleLogDebug(newsEntry, 2)
    consoleLogDebug(this.state.entry._id, 2)
    consoleLogDebug({"entryId":this.state.entry._id, "newsEntry":newsEntry, "newsClass":newsClass} )
  
    let success
    await axios.post( 
        config.URL_BASE + `world_entries/world/update/news` ,  
        {"entryId":this.state.entry._id, "newsEntry":newsEntry, "newsClass":newsClass} 
      ).then( (response) => {
      if (response.status < 300 ) {
          consoleLogDebug(`Creación exitosa de ${toFancyNewsClass(newsClass)}`, 1)
          success = true;
      } else {
          consoleLogDebug(`Error en creación de ${toFancyNewsClass(newsClass)}`);
          success = false;
      }
    }).catch( (err) =>{
      success = false;  
      consoleLogDebug('Error al enviar ' + toFancyNewsClass(newsClass));
      reject(err);
    })
  
    resolve( success )
  })}

  render(){
    return (

      <InnerLeftLayout style={{ ...scrollStyle }} >

        <div id={"subsectionTitle"} style={{...rowFlexStyle, marginTop:"0px"}}>
            <h1 style={{display:"inline-block"}}>  
                Difusión <NewsIcon/>
            </h1>

            <EntryChipField style={{display:"inline-block", marginLeft:"20px"}}
                entry={this.state.entry}
                changePage={this.props.changePage}
            />
        </div>

        <Diffusion newsEntries={this.state.entry.diffusion} changePage={this.props.changePage} send={this.send}/>

        <h1 style={{...rowFlexStyle, marginTop:"1em"}}>  
          Testimonios <BoardIcon/>
        </h1>

        <Testimonies newsEntries={this.state.entry.testimonies} changePage={this.props.changePage} send={this.send}/>

        <h1 style={{...rowFlexStyle, marginTop:"1em"}}>  
          Listado <SearchIcon/>
        </h1>

        <ShowHideWrapper show={this.state.entry.diffusion !== []}>
          {this.state.entry.diffusion.map( (diffusion) => (
            <NewsListItem style={{...rowFlexStyle, width:"60%"}}
              newsEntry={diffusion}
              changePage={this.props.changePage}
              newsClass={"diffusion"}
            />
          ))}
        </ShowHideWrapper>

        <ShowHideWrapper show={this.state.entry.testimonies !== []}>
          {this.state.entry.testimonies.map( (testimony) => (
            <NewsListItem style={{...rowFlexStyle, width:"60%"}}
              newsEntry={testimony}
              changePage={this.props.changePage}
              newsClass={"testimonies"}
            />
          ))}
        </ShowHideWrapper>

      </InnerLeftLayout>

    )
  }
}

export { News };
export { default_event }