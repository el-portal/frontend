import React, { Component } from 'react'
import { consoleLogDebugCreator } from "../../../../logger"
import { ThemeContext } from "../../../../context"
import { scrollStyle, headerStyle, divFlexStyle, rowFlexStyle, rowASym2FlexStyle, rowASym3FlexStyle, rowButtonFlexStyle, colorGreen } from "../../style"
import { TinyDescriptionField, MidDescriptionField, BinaryField, HighMediumLowField, TagsField } from "../../components/create_components"
import { getReducedOptionLabel, getReducedOptions, checkEntry } from '../create/create_methods'
import { ParagraphField, EntryListItem, ChipField, EntryChipField } from "../../components/show_components"
import { toSingEntryClass, default_individuals, toFancyEntryClass } from "../../components/world_entry_consts"
import { EventIcon, NewsIcon, BoardIcon, SearchIcon } from "../../components/icon_components"
import { ShowHideWrapper, BinaryOscillatingWrapper, toDateFormatted } from "../../components/news_components"
import { default_event } from "./news"
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import { Typography } from '@material-ui/core';

const verbosityLevel = 2;
const consoleLogDebug = consoleLogDebugCreator(verbosityLevel);

const default_empty_testimony = {
  verisimilitude: "",
  content: "",
  witness: '',
  witnessIndividual: [],
  isIndividual: false,
  created_ts: Date.now()
}

const requiredFields = ["verisimilitude", "content", ["witness", "witnessIndividual"]]

class Testimonies extends Component {
  static contextType = ThemeContext

  newsClass = "testimonies"
  defaultEmpty = default_empty_testimony
  requiredFields = requiredFields
  diffusionMax = 1

  constructor(props) {
      super(props);

      const filledNewsEntries = []
      for (const newsEntry of props.newsEntries) {
          let filledNewsEntry = {}
          for (const key of Object.keys(this.defaultEmpty)) {
              try {
                  if (typeof newsEntry[key] === 'undefined') {
                      filledNewsEntry[key] = this.defaultEmpty[key]
                  } else {
                      filledNewsEntry[key] = newsEntry[key]
                  }
                  } catch {
                      filledNewsEntry[key] = this.defaultEmpty[key]
              }
          }
          filledNewsEntries.push(filledNewsEntry)
      }

      this.state = {
          newsEntries: filledNewsEntries,
          createAvailable: false,
          ...this.defaultEmpty,
      }
  
      this.getOptions = this.getOptions.bind(this)
      this.isCreateAvailable = this.isCreateAvailable.bind(this)
      this.changeValue = this.changeValue.bind(this)
      this.clear = this.clear.bind(this)
      this.submit = this.submit.bind(this)
  }

  componentDidMount(){
    consoleLogDebug("Montando difusión")
    this.isCreateAvailable()
    this.getOptions()
  }

  async getOptions() {
    let individualsList 
    try {
      individualsList = await getReducedOptions( 'individuals', "witnessIndividual" )
    } catch {
      individualsList = [{name:"Frodo"}]
    }
    this.setState({witnessIndividualOptions: individualsList})
  }

  isCreateAvailable() {
    this.setState({createTestimony: true})
  }

  clear () {
    this.setState({...this.defaultEmpty})
  }
  
  changeValue ( event, value ) {
      let id
      try { // Case needed for Select component of Materials-UI
      id = value.props.id.split("-")[0];
      consoleLogDebug(`Caso 1: ${id} - ${event.target.value}`, 1)
      this.setState({[id]: event.target.value})
      } catch {
      id = event.target.id.split("-")[0]
      if (value) { // Case needed for Autoselect component of Materials-UI
          consoleLogDebug(`Caso 2: ${id} - ${value}`, 1)
          this.setState({[id]: value})
      } else if (value !== false) { // Case needed for any Text Field component of Materials-UI ^^
          consoleLogDebug(`Caso 3: ${id} - ${event.target.value}`, 1)
          this.setState({[id]: event.target.value})
      } else { // Case needed for Bynary Field component
          consoleLogDebug(`Caso 4: ${id} - ${value}`, 1)
          this.setState({[id]: value})
      }
      }
  }

  async submit() {
      let newsEntry = {}
      for (const key of Object.keys(this.defaultEmpty)) { 
          try {
              if (this.state[key] !== this.defaultEmpty[key] || typeof this.state[key] === "boolean") {
                  newsEntry[key] = this.state[key] 
        newsEntry[key] = this.state[key] 
                  newsEntry[key] = this.state[key] 
              }
          } catch {
              consoleLogDebug(`Problem with key ${key}`)
          }
      }
      newsEntry.newsClass = this.newsClass
      try{
          if ( checkEntry(newsEntry, this.requiredFields) ) {
              let success = await this.props.send(newsEntry, this.newsClass)
              if ( success ) { 
        if ( success ) { 
              if ( success ) { 
                  const currentNewsEntries = this.state.newsEntries
                  currentNewsEntries.push( {...newsEntry, created_ts: Date.now()} )
                  this.setState({newsEntries: currentNewsEntries})
                  this.clear()
                  this.isCreateAvailable()
              }
          } 
      } 
          } 
      } catch(err){ consoleLogDebug(err)}
  }

  render(){
    return (

      <div>
        
        <ShowHideWrapper show={this.state.newsEntries !== []}>
          {this.state.newsEntries.map((testimony, index) => (
            <Paper id={`showTestimonies${index}`} elevation={3} style={{...rowFlexStyle, padding:"10px", width:"calc(100%-20px)"}}>
              
            <div id={`showTestimonies${index}Title`}>
              <Typography style={{color:'grey', display:"inline-block"}}>
                { toDateFormatted(testimony.created_ts) }
              </Typography>

              <BinaryOscillatingWrapper switch={testimony.isIndividual}>
                <div style={{display: 'inline-block', padding:'2px'}}>
                  <ChipField style={{paddingLeft:"20px", display:"inline-block"}}
                    id={"witnessShow"}
                    label={testimony.witness}
                  />
                </div>
                <EntryChipField style={{marginLeft:"20px", display:"inline-block"}}
                  id={"witnessIndividualShow"}
                  entry={testimony.witnessIndividual[0]}
                  changePage={this.props.changePage}
                />
              </BinaryOscillatingWrapper>
          
              <Typography style={{color:'grey', marginLeft:"15px", display:"inline-block"}}>
                <i>Verosimilitud {testimony.verisimilitude.toLowerCase()}</i>
              </Typography>
            </div>

            <Typography id={`showTestimonies${index}Content`} style={{marginTop:".5em"}}>
              <i>{testimony.content}</i>
            </Typography>

            </Paper>
          ))}
        </ShowHideWrapper>

        <ShowHideWrapper show={this.state.createTestimony}>
          <Paper id={"createTestimony"} elevation={3} style={{...rowFlexStyle, padding:"10px", width:"calc(100%-20px)"}}>

            <div id={"createTestimonyTitle"}>
              <Typography style={{color:'grey', display:"inline-block"}}>
                { toDateFormatted(new Date()) }
              </Typography>

              <Typography style={{marginLeft:"15px", display:"inline-block"}}>
                <u>Nuevo testimonio</u>
              </Typography>
            
              <Typography style={{color:'grey', marginLeft:"15px", display:"inline-block"}}>
                <i>Comentario sobre lo que presenciaste.</i>
              </Typography>
            </div>

            <div id="contentNSubmit" style={{display:'grid', gridTemplateColumns: '90% 10%', columnGap: '20px', ...rowButtonFlexStyle}}>
              <MidDescriptionField style={{...rowFlexStyle}}
                id={"content"}
                label={"Contenido"}
                value={this.state.content}
                onChange={this.changeValue}
              />

              <Button style={{ ...colorGreen, ...rowFlexStyle }}  variant="outlined" size="medium" onClick={this.submit}> Enviar </Button>
            </div>

            <div id={"createTestimonyOptions"}>
              <BinaryField
                id="isIndividual"
                label="Testigo identificado"
                style={rowASym3FlexStyle[0]}
                value={this.state.isIndividual}
                onChange={this.changeValue}
              />

              <BinaryOscillatingWrapper switch={this.state.isIndividual}>
                <TinyDescriptionField style={rowASym3FlexStyle[1]}
                  id="witness"
                  label="Testigo*"
                  value={this.state.witness}
                  onChange={this.changeValue}
                />
                <TagsField style={rowASym3FlexStyle[1]}
                  id="witnessIndividual"
                  label="Testigo*"
                  options={this.state.witnessIndividualOptions}
                  getOptionLabel={getReducedOptionLabel}
                  value={this.state.witnessIndividual}
                  onChange={this.changeValue}
                />
              </BinaryOscillatingWrapper>

              <HighMediumLowField
                id="verisimilitude"
                label="Verosimilitud*"
                isMasculine={false}
                style={rowASym3FlexStyle[2]}
                value={this.state.verisimilitude}
                onChange={this.changeValue}
              />
            </div>

          </Paper>
        </ShowHideWrapper>

      </div>
    )
  }
}

export { Testimonies };
export { default_empty_testimony }