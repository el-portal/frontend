import React, { Component } from 'react'
import { consoleLogDebugCreator } from "../../../../logger"
import { ThemeContext } from "../../../../context"
import { scrollStyle, headerStyle, divFlexStyle, rowFlexStyle, rowASym2TFlexStyle, rowButtonFlexStyle, colorGreen } from "../../style"
import { MidDescriptionField, SelectField, HighMediumLowField } from "../../components/create_components"
import { getReducedOptionLabel, getReducedOptions, checkEntry } from '../create/create_methods'
import { EntryChipField } from "../../components/show_components"
import { ShowHideWrapper, toDateFormatted } from "../../components/news_components"
import { NewsIcon } from "../../components/icon_components"
import { default_event, diffusion } from "./news"
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import { Typography } from '@material-ui/core';

const verbosityLevel = 2;
const consoleLogDebug = consoleLogDebugCreator(verbosityLevel);

const default_empty_diffusion = {
    diffusionType: "",
    verisimilitude: "",
    content: "",
    created_ts: Date.now()
}

const requiredFields = ["diffusionType", "verisimilitude", "content"]

const diffusionTypeOptions = ["Noticia", "Rumor", "Chisme"]

class Diffusion extends Component {
    static contextType = ThemeContext

    newsClass = "diffusion"
    defaultEmpty = default_empty_diffusion
    requiredFields = requiredFields
    diffusionMax = 1

    constructor(props) {
        super(props);

        const filledNewsEntries = []
        for (const newsEntry of props.newsEntries) {
            let filledNewsEntry = {}
            for (const key of Object.keys(this.defaultEmpty)) {
                try {
                    if (typeof newsEntry[key] === 'undefined') {
                        filledNewsEntry[key] = this.defaultEmpty[key]
                    } else {
                        filledNewsEntry[key] = newsEntry[key]
                    }
                    } catch {
                        filledNewsEntry[key] = this.defaultEmpty[key]
                }
            }
            filledNewsEntries.push(filledNewsEntry)
        }

        this.state = {
            newsEntries: filledNewsEntries,
            createAvailable: false,
            ...this.defaultEmpty,
        }
    
        this.isCreateAvailable = this.isCreateAvailable.bind(this)
        this.changeValue = this.changeValue.bind(this)
        this.clear = this.clear.bind(this)
        this.submit = this.submit.bind(this)
    }

    componentDidMount(){
        consoleLogDebug("Montando difusión")
        this.isCreateAvailable()
    }

    isCreateAvailable() {
        if (typeof this.state.newsEntries[0] === "undefined"){
            this.setState({createAvailable: true})    
        } else {
            this.setState({createAvailable: false})
        }
    }

    changeValue ( event, value ) {
        let id
        try { // Case needed for Select component of Materials-UI
        id = value.props.id.split("-")[0];
        consoleLogDebug(`Caso 1: ${id} - ${event.target.value}`, 1)
        this.setState({[id]: event.target.value})
        } catch {
        id = event.target.id.split("-")[0]
        if (value) { // Case needed for Autoselect component of Materials-UI
            consoleLogDebug(`Caso 2: ${id} - ${value}`, 1)
            this.setState({[id]: value})
        } else if (value !== false) { // Case needed for any Text Field component of Materials-UI ^^
            consoleLogDebug(`Caso 3: ${id} - ${event.target.value}`, 1)
            this.setState({[id]: event.target.value})
        } else { // Case needed for Bynary Field component
            consoleLogDebug(`Caso 4: ${id} - ${value}`, 1)
            this.setState({[id]: value})
        }
        }
    }

    clear () {
        this.setState({...this.defaultEmpty})
    }

    async submit() {
        let newsEntry = {}
        for (const key of Object.keys(this.defaultEmpty)) { 
            try {
                if (this.state[key] !== this.defaultEmpty[key] || typeof this.state[key] === "boolean") {
                    newsEntry[key] = this.state[key] 
                }
            } catch {
                consoleLogDebug(`Problem with key ${key}`)
            }
        }
        newsEntry.newsClass = this.newsClass
        try{
            if ( checkEntry(newsEntry, this.requiredFields) ) {
                let success = await this.props.send(newsEntry, this.newsClass)
                if ( success ) { 
                    const currentNewsEntries = this.state.newsEntries
                    currentNewsEntries.push( {...newsEntry, created_ts: Date.now()} )
                    this.setState({newsEntries: currentNewsEntries})
                    this.clear()
                    this.isCreateAvailable()
                }
            } 
        } catch(err){ consoleLogDebug(err)}
    }

    render(){
        return (
        <div>

            <ShowHideWrapper show={this.state.newsEntries !== []}>
                {this.state.newsEntries.map((diffusion, index) => (
                    <Paper id={"showDiffusion"} elevation={3} style={{...rowFlexStyle, padding:"10px", width:"calc(100%-20px)"}}>
                        
                        <div>
                            <Typography style={{display:"inline-block", color:'grey'}}>
                                { toDateFormatted(diffusion.created_ts) }
                            </Typography>
                            
                            <Typography style={{display:"inline-block", color:'grey', marginLeft:"15px"}}>
                                <i>Verosimilitud {diffusion.verisimilitude.toLowerCase()}</i>
                            </Typography>
                        </div>

                        <Typography style={{marginTop:".5em"}}>
                        <i>{diffusion.content}</i>
                        </Typography>

                    </Paper>
                ))}
            </ShowHideWrapper>

            <ShowHideWrapper show={this.state.createAvailable}>
                <Paper id={"createDiffusion"} elevation={3} style={{...rowFlexStyle, padding:"10px", width:"calc(100%-20px)"}}>

                    <div id="createDiffusionTitle">
                        <Typography style={{color:'grey', display:"inline-block"}}>
                            { toDateFormatted(new Date()) }
                        </Typography>

                        <Typography style={{marginLeft:"15px", display:"inline-block"}}>
                            <u>Nueva difusión</u>
                        </Typography>

                        <Typography style={{display:"inline-block", color:'grey', marginLeft:"15px"}}>
                            <i>Noticia o rumor para anunciar el evento.</i>
                        </Typography>
                    </div>

                    <div id="contentNSubmit" style={{display:'grid', gridTemplateColumns: '90% 10%', columnGap: '20px', ...rowButtonFlexStyle}}>
                    <MidDescriptionField style={{...rowFlexStyle}}
                        id={"content"}
                        label={"Contenido"}
                        value={this.state.content}
                        onChange={this.changeValue}
                    />

                    <Button style={{ ...colorGreen, ...rowFlexStyle }}  variant="outlined" size="medium" onClick={this.submit}> Enviar </Button>
                    </div>

                    <div id="createDiffusionOptions">
                        <SelectField style={{...rowASym2TFlexStyle[0]}}
                            id={"diffusionType"}
                            label={"Tipo de difusión"}
                            value={this.state.diffusionType}
                            selectValues={diffusionTypeOptions}
                            onChange={this.changeValue}
                        />

                        <HighMediumLowField
                            id="verisimilitude"
                            label="Verosimilitud*"
                            isMasculine={false}
                            style={rowASym2TFlexStyle[1]}
                            value={this.state.verisimilitude}
                            onChange={this.changeValue}
                        />
                    </div>

                </Paper>
            </ShowHideWrapper>

        </div>
        )
    }
}

export { Diffusion };