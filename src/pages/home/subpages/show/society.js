import React, { Component } from 'react'
import { ThemeContext } from "../../../../context"
import { scrollStyle, headerStyle, divFlexStyle, rowFlexStyle } from "../../style"
import { NamePublicDescriptionField, OneLineField, 
         ParagraphField, EntryChipListField } from "../../components/show_components"
import { toFancyEntryClass } from "../../components/world_entry_consts"
import { default_empty_society } from "../create/society"
import { consoleLogDebugCreator } from "../../../../logger"

const verbosityLevel = 1;
const consoleLogDebug = consoleLogDebugCreator(verbosityLevel);

class ShowSociety extends Component {
  static contextType = ThemeContext

  defaultEmpty = default_empty_society
  entryClass = "societies"

  constructor(props) {
    super(props);

    let filledEntry = {}
    for (const key of Object.keys(this.defaultEmpty)) {
      try {
        if (typeof props.entry[key] === 'undefined') {
          filledEntry[key] = this.defaultEmpty[key]
        } else {
          filledEntry[key] = props.entry[key]
        }
      } catch {
        filledEntry[key] = this.defaultEmpty[key]
      }
    }
    this.state = { ...filledEntry }
  }

  componentDidMount(){
    consoleLogDebug(`Mostrando ${toFancyEntryClass(this.entryClass)}: ${this.state.name}`, 1)
  }

  render(){
    return (
      <div style={{ ...scrollStyle, ...divFlexStyle }} >

        <NamePublicDescriptionField style={{ ...headerStyle, ...rowFlexStyle }}
          name={this.state.name}
          publicDescription={this.state.publicDescription}
          entryClass={this.entryClass}
        />
        
        <OneLineField style={{ ...headerStyle, ...rowFlexStyle }}
          id={"societyType"}
          label={"Tipo de sociedad"}
          value={this.state.societyType}
        />

        <OneLineField style={{ ...headerStyle, ...rowFlexStyle }}
          id={"danger"}
          label={"Peligro"}
          value={this.state.danger}
        />

        <OneLineField style={{ ...headerStyle, ...rowFlexStyle }}
          id={"influence"}
          label={"Influencia"}
          value={this.state.influence}
        />

        <OneLineField style={{ ...headerStyle, ...rowFlexStyle }}
          id={"members"}
          label={"Cantidad de miembros"}
          value={this.state.members}
        />

        <OneLineField style={{ ...headerStyle, ...rowFlexStyle }}
          id={"secretism"}
          label={"Secretismo"}
          value={this.state.secretism}
        />

        <ParagraphField style={{ ...headerStyle, ...rowFlexStyle }}
          id={"longDescription"}
          label={"Descripción privada"}
          isMasculine={false}
          value={this.state.longDescription}
        />

        <EntryChipListField style={{...rowFlexStyle, paddingTop:'1em'}}
            id={"relatedLocations"}
            label={"Locaciones relacionadas"}
            entryClass={"locations"}
            entryList={this.state.relatedLocations}
            changePage={this.props.changePage}
        />

        <EntryChipListField style={{...rowFlexStyle, paddingTop:'1em'}}
            id={"associatedIndividuals"}
            label={"Individuos asociados"}
            entryClass={"individuals"}
            entryList={this.state.associatedIndividuals}
            changePage={this.props.changePage}
        />

        <EntryChipListField style={{...rowFlexStyle, paddingTop:'1em'}}
            id={"relatedSocieties"}
            label={"Sociedades relacionadas"}
            entryClass={"societies"}
            entryList={this.state.relatedSocieties}
            changePage={this.props.changePage}
        />

        <EntryChipListField style={{...rowFlexStyle, paddingTop:'1em'}}
            id={"notableTreasure"}
            label={"Tesoro notable"}
            entryClass={"objects"}
            entryList={this.state.notableTreasure}
            changePage={this.props.changePage}
        />
    
      </div>
    )
  }
}

export default ShowSociety;
