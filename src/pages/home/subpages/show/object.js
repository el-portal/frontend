import React, { Component } from 'react'
import { ThemeContext } from "../../../../context"
import { scrollStyle, headerStyle, divFlexStyle, rowFlexStyle } from "../../style"
import { NamePublicDescriptionField, OneLineField, 
         ParagraphField, TrueFalseField, EntryChipListField } from "../../components/show_components"
import { toFancyEntryClass } from "../../components/world_entry_consts"
import { default_empty_object } from "../create/object"
import { consoleLogDebugCreator } from "../../../../logger"

const verbosityLevel = 1;
const consoleLogDebug = consoleLogDebugCreator(verbosityLevel);

class ShowObject extends Component {
  static contextType = ThemeContext

  defaultEmpty = default_empty_object
  entryClass = "objects"

  constructor(props) {
    super(props);

    let filledEntry = {}
    for (const key of Object.keys(this.defaultEmpty)) {
      try {
        if (typeof props.entry[key] === 'undefined') {
          filledEntry[key] = this.defaultEmpty[key]
        } else {
          filledEntry[key] = props.entry[key]
        }
      } catch {
        filledEntry[key] = this.defaultEmpty[key]
      }
    }
    this.state = { ...filledEntry }
  }

  componentDidMount(){
    consoleLogDebug(`Mostrando ${toFancyEntryClass(this.entryClass)}: ${this.state.name}`, 1)
  }

  render(){
    return (
      <div style={{ ...scrollStyle, ...divFlexStyle }} >

        <NamePublicDescriptionField style={{ ...headerStyle, ...rowFlexStyle }}
          name={this.state.name}
          publicDescription={this.state.publicDescription}
          entryClass={this.entryClass}
        />
        
        <OneLineField style={{ ...headerStyle, ...rowFlexStyle }}
          id={"influence"}
          label={"Influencia"}
          value={this.state.influence}
        />

        <OneLineField style={{ ...headerStyle, ...rowFlexStyle }}
          id={"danger"}
          label={"Peligro"}
          value={this.state.danger}
        />

        <OneLineField style={{ ...headerStyle, ...rowFlexStyle }}
          id={"objectType"}
          label={"Tipo de objeto"}
          value={this.state.objectType}
        />

        <TrueFalseField style={{ ...headerStyle, ...rowFlexStyle }}
          id={"magic"}
          label={"Mágico"}
          value={this.state.magic}
        />

        <OneLineField style={{ ...headerStyle, ...rowFlexStyle }}
          id={"magicRarity"}
          label={"Rareza mágica"}
          value={this.state.magicRarity}
          isMasculine={false}
        />

        <ParagraphField style={{ ...headerStyle, ...rowFlexStyle }}
          id={"longDescription"}
          label={"Descripción privada"}
          isMasculine={false}
          value={this.state.longDescription}
        />

        <EntryChipListField style={{...rowFlexStyle, paddingTop:'1em'}}
            id={"possibleLocations"}
            label={"Posibles locaciones"}
            entryClass={"locations"}
            entryList={this.state.possibleLocations}
            changePage={this.props.changePage}
        />

        <EntryChipListField style={{...rowFlexStyle, paddingTop:'1em'}}
            id={"possibleIndividualOwners"}
            label={"Posibles individuos poseedores"}
            entryClass={"individuals"}
            entryList={this.state.possibleIndividualOwners}
            changePage={this.props.changePage}
        />

        <EntryChipListField style={{...rowFlexStyle, paddingTop:'1em'}}
            id={"possibleSocietyOwners"}
            label={"Posibles sociedades poseedoras"}
            entryClass={"societies"}
            entryList={this.state.possibleSocietyOwners}
            changePage={this.props.changePage}
        />
    
      </div>
    )
  }
}

export default ShowObject;
