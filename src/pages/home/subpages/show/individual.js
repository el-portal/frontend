import React, { Component } from 'react'
import { ThemeContext } from "../../../../context"
import { scrollStyle, headerStyle, divFlexStyle, rowFlexStyle } from "../../style"
import { NamePublicDescriptionField, OneLineField, 
         ParagraphField, EntryChipListField } from "../../components/show_components"
import { toFancyEntryClass } from "../../components/world_entry_consts"
import { default_empty_individual } from "../create/individual"
import { consoleLogDebugCreator } from "../../../../logger"

const verbosityLevel = 2;
const consoleLogDebug = consoleLogDebugCreator(verbosityLevel);

class ShowIndividual extends Component {
  static contextType = ThemeContext

  defaultEmpty = default_empty_individual
  entryClass = "individuals"

  constructor(props) {
    super(props);

    let filledEntry = {}
    for (const key of Object.keys(this.defaultEmpty)) {
      try {
        if (typeof props.entry[key] === 'undefined') {
          filledEntry[key] = this.defaultEmpty[key]
        } else {
          filledEntry[key] = props.entry[key]
        }
      } catch {
        filledEntry[key] = this.defaultEmpty[key]
      }
    }
    this.state = { ...filledEntry }
  }

  componentDidMount(){
    consoleLogDebug(`Mostrando ${toFancyEntryClass(this.entryClass)}: ${this.state.name}`, 1)
  }

  render(){
    return (
      <div style={{ ...scrollStyle, ...divFlexStyle }} >

        <NamePublicDescriptionField style={{ ...headerStyle, ...rowFlexStyle }}
          name={this.state.name}
          publicDescription={this.state.publicDescription}
          entryClass={this.entryClass}
        />
        
        <OneLineField style={{ ...headerStyle, ...rowFlexStyle }}
          id={"race"}
          label={"Raza"}
          value={this.state.race}
        />

        <OneLineField style={{ ...headerStyle, ...rowFlexStyle }}
          id={"occupation"}
          label={"Ocupación"}
          value={this.state.occupation}
        />

        <OneLineField style={{ ...headerStyle, ...rowFlexStyle }}
          id={"influence"}
          label={"Influencia"}
          value={this.state.influence}
        />

        <OneLineField style={{ ...headerStyle, ...rowFlexStyle }}
          id={"danger"}
          label={"Peligro"}
          value={this.state.danger}
        />

        <ParagraphField style={{ ...headerStyle, ...rowFlexStyle }}
          id={"longDescription"}
          label={"Descripción privada"}
          isMasculine={false}
          value={this.state.longDescription}
        />

        <EntryChipListField style={{...rowFlexStyle, paddingTop:'1em'}}
            id={"usualLocations"}
            label={"Locaciones usuales"}
            entryClass={"locations"}
            entryList={this.state.usualLocations}
            changePage={this.props.changePage}
        />

        <EntryChipListField style={{...rowFlexStyle, paddingTop:'1em'}}
            id={"relatedIndividuals"}
            label={"Individuos relacionados"}
            entryClass={"individuals"}
            entryList={this.state.relatedIndividuals}
            changePage={this.props.changePage}
        />

        <EntryChipListField style={{...rowFlexStyle, paddingTop:'1em'}}
            id={"associatedSocieties"}
            label={"Sociedades a las que pertenece"}
            entryClass={"societies"}
            entryList={this.state.associatedSocieties}
            changePage={this.props.changePage}
        />

        <EntryChipListField style={{...rowFlexStyle, paddingTop:'1em'}}
            id={"notableInventory"}
            label={"Inventario notable"}
            entryClass={"objects"}
            entryList={this.state.notableInventory}
            changePage={this.props.changePage}
        />
    
      </div>
    )
  }
}

export default ShowIndividual;
