import React, { Component } from 'react'
import { ThemeContext } from "../../../../context"
import { scrollStyle, headerStyle, divFlexStyle, rowFlexStyle } from "../../style"
import { NamePublicDescriptionField, OneLineField, 
         ParagraphField, EntryChipListField } from "../../components/show_components"
import { toFancyEntryClass } from "../../components/world_entry_consts"
import { default_empty_event } from "../create/event"
import { NewsTextField } from "../../components/news_components"
import { consoleLogDebugCreator } from "../../../../logger"

const verbosityLevel = 1;
const consoleLogDebug = consoleLogDebugCreator(verbosityLevel);

class ShowEvent extends Component {
  static contextType = ThemeContext

  defaultEmpty = default_empty_event
  entryClass = "events"

  constructor(props) {
    super(props);

    let filledEntry = {}
    for (const key of Object.keys(this.defaultEmpty)) {
      try {
        if (typeof props.entry[key] === 'undefined') {
          filledEntry[key] = this.defaultEmpty[key]
        } else {
          filledEntry[key] = props.entry[key]
        }
      } catch {
        filledEntry[key] = this.defaultEmpty[key]
      }
    }
    filledEntry.entryClass = this.entryClass
    this.state = { ...filledEntry }
  }

  componentDidMount(){
    consoleLogDebug(`Mostrando ${toFancyEntryClass(this.entryClass)}: ${this.state.name}`, 1)
    consoleLogDebug(this.props.entry)
    consoleLogDebug(this.state)
  }

  render(){
    return (
      <div style={{ ...scrollStyle, ...divFlexStyle }} >

        <NamePublicDescriptionField style={{ ...headerStyle, ...rowFlexStyle }}
          name={this.state.name}
          publicDescription={this.state.publicDescription}
          entryClass={this.entryClass}
        />
        
        <OneLineField style={{ ...headerStyle, ...rowFlexStyle }}
          id={"impact"}
          label={"Impacto"}
          value={this.state.impact}
        />

        <NewsTextField style={{ ...headerStyle, ...rowFlexStyle }}
          id={"news"}
          label={"Noticias"}
          entryId={this.props.entry._id}
          diffusion={this.state.diffusion}
          testimonies={this.state.testimonies}
          changePage={this.props.changePage}
        />

        <EntryChipListField style={{...rowFlexStyle, paddingTop:'1em'}}
            id={"happeedIn"}
            label={"Sitio/s en el/los que ocurrió"}
            entryClass={"locations"}
            entryList={this.state.happenedIn}
            changePage={this.props.changePage}
        />

        <ParagraphField style={{ ...headerStyle, ...rowFlexStyle }}
            id={"keyInformation"}
            label={"Información clave"}
            isMasculine={true}
            value={this.state.keyInformation}
        />

        <ParagraphField style={{ ...headerStyle, ...rowFlexStyle }}
            id={"longDescription"}
            label={"Descripción privada"}
            isMasculine={false}
            value={this.state.longDescription}
        />

        <EntryChipListField style={{...rowFlexStyle, paddingTop:'1em'}}
            id={"involvedIndividuals"}
            label={"Individuos involucrados"}
            entryClass={"individuals"}
            entryList={this.state.involvedIndividuals}
            changePage={this.props.changePage}
        />

        <EntryChipListField style={{...rowFlexStyle, paddingTop:'1em'}}
            label={"Sociedades involucradas"}
            entryClass={"societies"}
            entryList={this.state.involvedSocieties}
            changePage={this.props.changePage}
        />

        <EntryChipListField style={{...rowFlexStyle, paddingTop:'1em'}}
            label={"Objetos notables involucrados"}
            entryClass={"objects"}
            entryList={this.state.involvedObjects}
            changePage={this.props.changePage}
        />
    
        <ParagraphField style={{ ...headerStyle, ...rowFlexStyle }}
            id={"changes"}
            label={"Cambios ocasionados"}
            isMasculine={true}
            value={this.state.changes}
        />

      </div>
    )
  }
}

export default ShowEvent;
