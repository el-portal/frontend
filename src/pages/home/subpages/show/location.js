import React, { Component } from 'react'
import { ThemeContext } from "../../../../context"
import { scrollStyle, headerStyle, divFlexStyle, rowFlexStyle, rowSym2FlexStyle, colorGreen } from "../../style"
import { NamePublicDescriptionField, OneLineField, 
         ParagraphField, EntryChipListField } from "../../components/show_components"
import { toFancyEntryClass } from "../../components/world_entry_consts"
import { default_empty_location } from "../create/location"
import { consoleLogDebugCreator } from "../../../../logger"

const verbosityLevel = 1;
const consoleLogDebug = consoleLogDebugCreator(verbosityLevel);

class ShowLocation extends Component {
  static contextType = ThemeContext

  defaultEmpty = default_empty_location
  entryClass = "locations"

  constructor(props) {
    super(props);

    let filledEntry = {}
    for (const key of Object.keys(this.defaultEmpty)) {
      try {
        if (typeof props.entry[key] === 'undefined') {
          filledEntry[key] = this.defaultEmpty[key]
        } else {
          filledEntry[key] = props.entry[key]
        }
      } catch {
        filledEntry[key] = this.defaultEmpty[key]
      }
    }
    this.state = { ...filledEntry }
  }

  componentDidMount(){
    consoleLogDebug(`Mostrando ${toFancyEntryClass(this.entryClass)}: ${this.state.name}`, 1)
  }

  render(){
    return (
      <div style={{ ...scrollStyle, ...divFlexStyle }} >

        <NamePublicDescriptionField style={{ ...headerStyle, ...rowFlexStyle }}
          name={this.state.name}
          publicDescription={this.state.publicDescription}
          entryClass={this.entryClass}
        />
        
        <OneLineField style={{ ...headerStyle, ...rowFlexStyle }}
          id={"locationType"}
          label={"Tipo de locación"}
          value={this.state.locationType}
        />

        <OneLineField style={{ ...headerStyle, ...rowFlexStyle }}
          id={"fame"}
          label={"Fama"}
          value={this.state.fame}
        />

        <OneLineField style={{ ...headerStyle, ...rowFlexStyle }}
          id={"danger"}
          label={"Peligro"}
          value={this.state.danger}
        />

        <ParagraphField style={{ ...headerStyle, ...rowFlexStyle }}
          id={"longDescription"}
          label={"Descripción privada"}
          isMasculine={false}
          value={this.state.longDescription}
        />

        <EntryChipListField style={{...rowFlexStyle, paddingTop:'1em'}}
            id={"relatedIndividuals"}
            label={"Individuos relacionados"}
            entryClass={"individuals"}
            entryList={this.state.relatedIndividuals}
            changePage={this.props.changePage}
        />

        <EntryChipListField style={{...rowFlexStyle, paddingTop:'1em'}}
            id={"relatedSocieties"}
            label={"Sociedades relacionadas"}
            entryClass={"societies"}
            entryList={this.state.relatedSocieties}
            changePage={this.props.changePage}
        />

        <EntryChipListField style={{...rowFlexStyle, paddingTop:'1em'}}
            id={"notableTreasure"}
            label={"Tesoros notables"}
            entryClass={"objects"}
            entryList={this.state.notableTreasure}
            changePage={this.props.changePage}
        />

        <EntryChipListField style={{...rowFlexStyle, paddingTop:'1em'}}
            id={"includedInLocations"}
            label={"Locaciones que incluyen a esta locación"}
            entryClass={"locations"}
            entryList={this.state.includedInLocations}
            changePage={this.props.changePage}
        />

        <EntryChipListField style={{...rowFlexStyle, paddingTop:'1em'}}
            id={"includesLocations"}
            label={"Locaciones incluídas dentro de esta locación"}
            entryClass={"locations"}
            entryList={this.state.includesLocations}
            changePage={this.props.changePage}
        />
    
      </div>
    )
  }
}

export default ShowLocation;
