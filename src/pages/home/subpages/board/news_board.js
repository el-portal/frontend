import React, { Component } from 'react'
import { ThemeContext } from "../../../../context"
import { scrollStyle, headerStyle, divFlexStyle, rowFlexStyle } from "../../style"
import { NewsIcon } from "../../components/icon_components"
import { EntryListField } from "../../components/show_components"
import FloatingSocialButtons from '../../floating_social_buttons'
import axios from 'axios'
import { config } from "../../../../config" 

class NewsBoard extends Component {
  static contextType = ThemeContext
  constructor(props) {
    super(props);
    this.state = {
      mostRecentEntries: []
    }
    this.getRecentEntries = this.getRecentEntries.bind(this)
  }

  componentDidMount(){
    console.log('Montando tablón')
    this.getRecentEntries()
  }

  getRecentEntries(){
    axios.post( config.URL_BASE + '/world_entries/world/recent/entries' ).then( (response) => {
      this.setState({mostRecentEntries: response.data})
      console.log('Éxito al recuperar las entradas más recientes')
      console.log(`La primera es: ${response.data[0].name}`) 
    }).catch ((err)=>{
      console.log('Error al recuperar las entradas más recientes') 
      console.log(err)   
    })
  }

  render(){
    return (

      <div style={{ ...divFlexStyle, height:'100%' }} >

        <h1 style={{ ...headerStyle, ...rowFlexStyle, textAlign:'center' }}> Tablón </h1>

        <h2 style={{ ...headerStyle, ...rowFlexStyle }}>
          <NewsIcon fontSize="large" style={{margin:'0 .8em 0 0.2em'}}/>
          Novedades <i>on rol</i>
        </h2>

        <EntryListField
          entryList={this.state.mostRecentEntries}
          changePage={this.props.changePage}
          errorLabel={"¡Uy, lo sentimos! No se encontraron resultados que mostrar."}
          style={{...scrollStyle, height:'68%'}}
        />

        <FloatingSocialButtons/>

      </div>
    )
  }
}

export default NewsBoard;
