import React, { Component } from 'react'
import { InnerLeftLayout } from '../layouts';
import { SettingsIcon } from "../components/icon_components"

class Configuration extends Component {
  constructor(props) {
    super(props);
    this.state = {
    }
  }

  componentDidMount(){
    console.log('Montando configuracion')
  }

  render(){
    return (
      <InnerLeftLayout>
        <h1 style={{textAlign:'left'}}>
          Configuración
          <SettingsIcon style={{marginLeft:'0.2em'}} fontSize='large'/>
        </h1>
      </InnerLeftLayout>
    )
  }
}

export default Configuration;
