import React, { Component } from 'react'
import map from '../../maps/MapaNacho.jpg';
import { ThemeContext } from "../../../../context"
import Ciudad from "./svgs"
import axios from 'axios'
import { config } from "../../../../config" 

class Map extends Component {
  static contextType = ThemeContext
  constructor(props) {
    super(props);
    this.state = {
      locations : []
    }
  }

  componentDidMount(){
    console.log('Montando mapa')
    this.getLocations()
  }

  async getLocations(){
    await axios.post( config.URL_BASE + "/world_entries/entries/get/class", {entryClass:"locations"} ).then( (response) => {
      console.log(response.data)
      this.setState({locations: response.data})
      console.log('Éxito al recuperar las locaciones para el mapa')
      console.log(`La primera es ${response.data[0].name} con posición ${response.data[0].x}, ${response.data[0].y}`)
    }).catch ((err)=>{
      console.log('Error al recuperar las locaciones para el mapa') 
      this.setState({locations: []})
      console.log(err)   
    })
  }

  displayLocations(){
    return(
      <div>
        {this.state.locations.map( location =>{
          return(
            <div style={{ 
              position: "absolute",
              zIndex:10, 
              cursor:"pointer",
              width:"10px", 
              height:"10px", 
              top: location.y + "%",
              left: location.x + "%",
            }}>
              <div style={{
                color:"white",
                "textShadow": "-2px 0 black, 0 2px black, 2px 0 black, 0 -2px black"
              }}>
                {location.name}
              </div>
              <Ciudad/>
            </div>
          )
        })}
      </div>
    )
  }

  render(){
    return (
      <div style={{backgroundColor:this.context.theme.content_background, height:'100%', width:'100%', display:'flex'}}>
        <div style={{ height:'100%', width:'100%', margin:'auto', backgroundColor:'green', position: "relative"}}>
          <img alt="Mapa" src={map} width="100%" height="100%"/>
          {this.displayLocations()}
        </div> 
      </div>
    )
  }
}

export default Map;
