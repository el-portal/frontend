import React, { Component } from 'react'
import { ThemeContext } from "../../../context"
import { InnerLeftLayout, TopLeftLayout, BottomLeftLayout } from '../layouts'
import { divSearchColFlexStyle, rowButtonFlexStyle,  colorGreen, scrollStyle } from "../style"
import { SearchField } from "../components/create_components"
import axios from 'axios'
import { EntryListField } from '../components/show_components'
import { SearchIcon } from "../components/icon_components"
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import { config } from "../../../config" 
import { consoleLogDebugCreator } from "../../../logger"

const verbosityLevel = 0;
const consoleLogDebug = consoleLogDebugCreator(verbosityLevel);

// const useStyles = makeStyles((theme) => ({
//   root: {
//     display: 'flex',
//   },
//   formControl: {
//     margin: theme.spacing(3),
//   },
// }));

class Search extends Component {
  static contextType = ThemeContext
  // classes = useStyles();
  constructor(props) {
    super(props);
    this.state = {
      searchString: '',
      filters:{
        events: true,
        locations: true,
        individuals: true,
        societies: true,
        objects: true,
      },
      searchedString: '',
      searchedResults: null,
    }
    this.changeSearchString = this.changeSearchString.bind(this)
    this.changeFilter = this.changeFilter.bind(this)

    this.checkSubmit = this.checkSubmit.bind(this)
    this.submit = this.submit.bind(this)
  }

  componentDidMount(){
    consoleLogDebug('Montando buscar')
  }

  checkSubmit() {
    const requiredTextFields = []
    const requiredArrayFields = []
    
    let verifiedTextFields = requiredTextFields.every( t  => t !== '');
    let verifiedArrayFields = requiredArrayFields.every( a  => a !== []);
    
    return verifiedTextFields && verifiedArrayFields
  }

  async submit() {
    try{
      if (this.checkSubmit()) {

        const thisSearchedString = this.state.searchString
        this.setState({'searchedString': thisSearchedString})

        const filters = []
        for (const entryClass of Object.keys(this.state.filters)) {
          if (this.state.filters[entryClass]) {
            filters.push( {"entryClass": entryClass} )
          }
        }

        let search = {
          searchString: thisSearchedString,
          filters: filters,
        }

        await axios.post( config.URL_BASE + '/world_entries/entries/search',  search ).then( (response) => {
          if(response.status < 300 ){
            console.log('Búsqueda realizada con éxito')
            console.log(response.data)
            this.setState({ searchedResults: response.data})
          } else {
            console.log(`Error al realizar la búsqueda`);
          }
        }).catch( (err) =>{
          console.log('Error al enviar búsqueda');
          console.log(err);
        })
        return
      } else {
        console.log("¡Hay campos requeridos vacíos!")
        return
      } 
    } catch(err){ console.log(err)}
  }

  changeSearchString (event) {
    this.setState({searchString:event.target.value});
  };

  changeFilter(filterType) {
    var newFilters = {...this.state.filters}
    newFilters[filterType] = !this.state.filters[filterType]
    this.setState({filters: newFilters});
  }

  render(){
    return (
      <InnerLeftLayout>
        <TopLeftLayout>

        <h1 id="title" style={{textAlign:'left'}}>
          Buscador 
          <SearchIcon style={{marginLeft:'0.2em'}} fontSize='large'/>
        </h1>

        <div style={{
          margin:'auto',
          width:'100%', 
        }}
        >

          <div id="searchBoxDiv" style={{
            ...divSearchColFlexStyle,
            marginTop:'0px',
          }}>

            <div style={{
              display:'grid', 
              gridTemplateColumns: '90% 10%',
              columnGap: '20px',
              ...rowButtonFlexStyle
            }}>

              <SearchField
                id="searchText"
                label="Texto a buscar"
                // style={rowFlexStyle}
                value={this.state.searchString} 
                onChange={this.changeSearchString}
                // style={{...rowButtonFlexStyle}}
              />

              <Button style={{ ...colorGreen }}  variant="outlined" size="medium" onClick={this.submit}> Buscar </Button>

            </div>
            <FormControl component="fieldset">
              <FormGroup row={true}>
                  <FormControlLabel 
                    control={<Checkbox color="default" checked={this.state.filters.events} onChange={() => this.changeFilter("events")} name="events" />}
                    label="Eventos"
                  />
                  <FormControlLabel 
                    control={<Checkbox color="default" checked={this.state.filters.locations} onChange={() => this.changeFilter("locations")} name="locations" />}
                    label="Locaciones"
                  />
                  <FormControlLabel 
                    control={<Checkbox color="default" checked={this.state.filters.individuals} onChange={() => this.changeFilter("individuals")} name="individuals" />}
                    label="Individuos"
                  />
                  <FormControlLabel 
                    control={<Checkbox color="default" checked={this.state.filters.societies} onChange={() => this.changeFilter("societies")} name="societies" />}
                    label="Sociedades"
                  />
                  <FormControlLabel 
                    control={<Checkbox color="default" checked={this.state.filters.objects} onChange={() => this.changeFilter("objects")} name="objects" />}
                    label="Objetos"
                  />
              </FormGroup>
            </FormControl>
          </div>
        </div>

        </TopLeftLayout>

        <BottomLeftLayout style={{...scrollStyle}}>
          <EntryListField
            entryList={this.state.searchedResults}
            changePage={this.props.changePage}
            errorLabel={"¡Uy, lo sentimos! No se encontraron resultados que mostrar."}
          />
        </BottomLeftLayout>
      </InnerLeftLayout>
    )
  }
}

export default Search;
