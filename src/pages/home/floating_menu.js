import { green } from '@material-ui/core/colors';
import React, { Component, useState, useContext } from 'react'
import { ThemeContext } from "../../context"
import { EventIcon, LocationIcon, IndividualIcon, SocietyIcon, ObjectIcon } from './components/icon_components'
import { WorldIcon, MapIcon, BoardIcon, SearchIcon } from './components/icon_components'

class FloatingMenu extends Component {
    constructor(props) {
        super(props);
    }

    render(){ 
        return (
            <div>
                <ThemeContext.Consumer>
                    {value => (
                        <nav id="floatingMenu" style={{
                            color: value.theme.menu_color,
                            paddingTop: "5px",
                            backgroundColor: value.theme.menu_background,
                            position: "absolute",
                            top:0,
                            left:0,
                            bottom:0,
                            zIndex: '100',
                            justifyContent: "space-between",

                        }}>
                            <SeeBoardButton changePage={this.props.changePage} page={this.props.page}/>
                            <SeeMapButton changePage={this.props.changePage} page={this.props.page}/>
                            <CreateEventButton changePage={this.props.changePage} page={this.props.page}/>
                            <CreateLocationButton changePage={this.props.changePage} page={this.props.page}/>
                            <CreateIndividualButton changePage={this.props.changePage} page={this.props.page}/>
                            <CreateSocietyButton changePage={this.props.changePage} page={this.props.page}/>
                            <CreateObjectButton changePage={this.props.changePage} page={this.props.page}/>
                            <SearchButton changePage={this.props.changePage} page={this.props.page}/>
                        </nav>
                    )}
                </ThemeContext.Consumer>
            </div>
        )
    }
}

export function FloatingMenuItemButton(props) {
    const [hover, setHover] = useState(false);
    const { theme } = useContext(ThemeContext);
  
    return (
      <div {...props}>
        <div
          style={{
            textAlign: 'center',
            display: "flex",
            cursor: 'pointer',
            paddingBottom:'5%',
            paddingTop:'5%',
            fontSize: '120%',
            alignItems: "center",
            ...props.style
          }}
          onMouseEnter={() => setHover(true)}
          onMouseLeave={() => setHover(false)}
          onClick={props.onClick}
        >
            <props.Icon style={{
                padding: "0.5em",
                color: props.selected ? "white" : "grey"
            }}/>
        </div>
        {hover ? (
          <div style={{ position: "relative", display: "inline" }}>
            <div
              style={{
                width: 0,
                height: 0,
                borderTop: "6px solid transparent",
                borderRight: "6px solid " + theme.menu_color,
                borderBottom: "6px solid transparent",
                color: theme.menu_color,
                position: "absolute",
                backgroundColor: "transparent",
                left: "46px",
                top: "-27px",
                zIndex: "2",
              }}
            ></div>
            <div
              style={{
                color: theme.menu_color,
                position: "absolute",
                backgroundColor: theme.menu_background,
                left: "52px",
                zIndex: "2",
                top: "-35px",
                paddingTop: "4px",
                paddingBottom: "4px",
                paddingLeft: "8px",
                paddingRight: "8px",
                borderRadius: "5px",
                border: "1px solid " + theme.menu_color,
                width:"fit-content",
                
              }}
            >
            {props.label}
            </div>
          </div>
        ) : (
          <div />
        )}
      </div>
    );
  }

/*  
const FloatingMenuItemButton = props => (
    <ThemeContext.Consumer>
        {value => (
            <div onClick={props.onClick} style={{
                textAlign: 'center',
                display: "flex",
                cursor: 'pointer',
                paddingBottom:'5%',
                paddingTop:'5%',
                fontSize: '120%',
                alignItems: "center",
                ...props.style
            }}>
                <props.Icon style={{
                    padding: "0.5em",
                    color: props.selected ? "white" : "grey"
                }}/>
                {` ${props.label}`}
            </div>
        )}
    </ThemeContext.Consumer>
)
*/
const SeeBoardButton = props => (
    <FloatingMenuItemButton 
        label="Vér tablón"
        onClick={()=>props.changePage('board')}
        Icon={BoardIcon}
        style={props.style}
        selected={props.page === "board"}
    />
);

const SeeMapButton = props => (
    <FloatingMenuItemButton 
        label="Vér mapa"
        onClick={()=>props.changePage('map')}
        Icon={MapIcon}
        style={props.style}
        selected={props.page === "map"}
    />
);

const CreateEventButton = props => (
    <FloatingMenuItemButton 
        label="Crear evento" 
        onClick={()=>props.changePage('create-event')}
        Icon={EventIcon}
        style={props.style}
        selected={props.page === "create-event"}
    />
);

const CreateLocationButton = props => (
    <FloatingMenuItemButton 
        label="Crear locación" 
        onClick={()=>props.changePage('create-location')}
        Icon={LocationIcon}
        style={props.style}
        selected={props.page === "create-location"}
    />
);

const CreateIndividualButton = props => (
    <FloatingMenuItemButton 
        label="Crear individuo" 
        onClick={()=>props.changePage('create-individual')}
        Icon={IndividualIcon}
        style={props.style}
        selected={props.page === "create-individual"}
    />
);

const CreateSocietyButton = props => (
    <FloatingMenuItemButton 
        label="Crear sociedad" 
        onClick={()=>props.changePage('create-society')}
        Icon={SocietyIcon}
        style={props.style}
        selected={props.page === "create-society"}
    />
);

const CreateObjectButton = props => (
    <FloatingMenuItemButton 
        label="Crear objeto" 
        onClick={()=>props.changePage('create-object')}
        Icon={ObjectIcon}
        style={props.style}
        selected={props.page === "create-object"}
    />
);

const SeeTestimoniesButton = props => (
    <FloatingMenuItemButton 
        label="Noticias" 
        onClick={()=>props.changePage('news')}
        Icon={SearchIcon}
        style={props.style}
        selected={props.page === "news"}
    />
);

const SearchButton = props => (
    <FloatingMenuItemButton 
        label="Buscar" 
        onClick={()=>props.changePage('search')}
        Icon={SearchIcon}
        style={props.style}
        selected={props.page === "search"}
    />
);

// const ShowButton = props => (
//     <FloatingMenuItemButton 
//         label="Ver Objeto" 
//         onClick={()=>props.changePage('show-object')}
//         Icon={ObjectIcon}
//         style={props.style}
//     />
// );

export default FloatingMenu