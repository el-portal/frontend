
const consoleLogDebugCreator = (verbosityLevel) => {
    const consoleLogDebugInsider = (stringToConsoleLog, importanceLevel=0) => {
      if ( importanceLevel <= verbosityLevel ) { console.log( stringToConsoleLog ) }
    }
    return ( consoleLogDebugInsider )
  }

export {consoleLogDebugCreator}